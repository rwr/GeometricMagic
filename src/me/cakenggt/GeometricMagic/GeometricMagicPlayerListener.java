/**
 * GeometricMagic allows players to draw redstone circles on the ground to do things such as teleport and transmute blocks.
 * Copyright (C) 2012  Alec Cox (cakenggt), Andrew Stevanus (Hoot215) <hoot893@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cakenggt.GeometricMagic;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import net.h31ix.anticheat.api.AnticheatAPI;
import net.h31ix.anticheat.manage.CheckType;
import net.milkbowl.vault.economy.Economy;

import regalowl.hyperconomy.HyperObjectAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.PluginManager;

public class GeometricMagicPlayerListener implements Listener {
	static GeometricMagic plugin = new GeometricMagic();
	private static GeometricMagicBlockListener blockListener;
	private static HashMap<String, String> setCircleArray = new HashMap<String, String>();

	public GeometricMagicPlayerListener(GeometricMagic instance, GeometricMagicBlockListener blockListenerInstance) {
		plugin = instance;
		blockListener = blockListenerInstance;

		setCircleArray.put("1111", "Item Circle");
		setCircleArray.put("1133", "Repair Circle");
		setCircleArray.put("1222", "Conversion Circle");
		setCircleArray.put("1233", "Philosopher's Stone Circle");
		setCircleArray.put("1234", "Boron Circle");
		setCircleArray.put("2223", "Soul Circle");
		setCircleArray.put("2224", "Homunculus Circle");
		setCircleArray.put("2244", "Safe Teleportation Circle");
		setCircleArray.put("2333", "Explosion Circle");
		setCircleArray.put("3334", "Fire Circle");
		setCircleArray.put("3344", "Fire Explosion Circle");
		setCircleArray.put("3444", "Human Transmutation Circle");
		setCircleArray.put("x111", "Bed Circle");
		setCircleArray.put("x044", "Pig Circle");
		setCircleArray.put("x144", "Sheep Circle");
		setCircleArray.put("x244", "Cow Circle");
		setCircleArray.put("x344", "Chicken Circle");

	}

	public static Economy economy = null;
	private static HashMap<String, Long> mapCoolDowns = new HashMap<String, Long>();

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerInteract(PlayerInteractEvent event) {

		// System.out.println("is playerinteractevent");
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK && event.getAction() != Action.RIGHT_CLICK_AIR) {
			// System.out.println("doesn't equal click block or click air");
			return;
		}

		Player player = event.getPlayer();

		boolean sacrificed = false;

		if (!player.hasPermission("geometricmagic.bypass.sacrifice")) {
			try {
				sacrificed = GeometricMagic.checkSacrificed(player);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

		boolean sacrifices = false;
		try {
			sacrifices = GeometricMagic.checkSacrifices(player);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		Block actBlock = player.getLocation().getBlock();

		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (event.getClickedBlock().getType() == Material.WORKBENCH && (sacrifices && !sacrificed) && !player.hasPermission("geometricmagic.bypass.crafting") && player.hasPermission("geometricmagic.set")) {
				// cancel event instead of turning block into air
				player.sendMessage("You have already sacrificed your crafting abilities. You must sacrifice your alchemy forever to get them back by performing another human transmutation.");
				event.setCancelled(true);
			}
			actBlock = event.getClickedBlock();
		}

		ItemStack inHand = event.getPlayer().getItemInHand();
		Material inHandType = inHand.getType();

		// do nothing if they don't have permission to use set circles
		if (!player.hasPermission("geometricmagic.set") && inHandType == Material.FLINT) {
			return;
		}

		if ((sacrificed && (inHandType == Material.FLINT || (actBlock.getType() == Material.REDSTONE_WIRE && inHand.getAmount() == 0))) && !player.hasPermission("geometricmagic.bypass.sacrifice")) {
			player.sendMessage("You have sacrificed your alchemy abilities forever.");
			return;
		}

		if (event.getAction() == Action.RIGHT_CLICK_AIR && inHandType == Material.FLINT && sacrifices) {
			actBlock = player.getTargetBlock(null, 120);
		}

		World world = player.getWorld();
		try {
			isCircle(player, world, actBlock, sacrifices);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void isCircle(Player player, World world, Block actBlock, Boolean sacrifices) throws IOException {
		// System.out.println("isCircle?");
		if (actBlock.getType() == Material.REDSTONE_WIRE && player.getItemInHand().getAmount() == 0) {
			// System.out.println("isCircle");
			circleChooser(player, world, actBlock);
		}
		if (player.getItemInHand().getType() == Material.FLINT && sacrifices) {

			File myFile = new File("plugins/GeometricMagic/sacrifices.txt");
			String circle = "[0, 0, 0, 0]";
			if (myFile.exists()) {
				Scanner inputFile = new Scanner(myFile);
				while (inputFile.hasNextLine()) {
					String name = inputFile.nextLine();
					if (name.equals(player.getName())) {
						circle = inputFile.nextLine();
					} else
						inputFile.nextLine();
				}
				inputFile.close();
			} else {
				return;
			}

			try {
				// exempt player from AntiCheat check
				if (Bukkit.getServer().getPluginManager().getPlugin("AntiCheat") != null) {
					AnticheatAPI.exemptPlayer(player, CheckType.FAST_PLACE);
					AnticheatAPI.exemptPlayer(player, CheckType.FAST_BREAK);
					AnticheatAPI.exemptPlayer(player, CheckType.LONG_REACH);
					AnticheatAPI.exemptPlayer(player, CheckType.NO_SWING);
				}

				if (circle.equals("0")) {
					circleChooser(player, world, actBlock);
				} else
					setCircleEffects(player, player.getWorld(), player.getLocation().getBlock(), actBlock, circle);

				// unexempt player from AntiCheat check
				if (Bukkit.getServer().getPluginManager().getPlugin("AntiCheat") != null) {
					AnticheatAPI.unexemptPlayer(player, CheckType.FAST_PLACE);
					AnticheatAPI.unexemptPlayer(player, CheckType.FAST_BREAK);
					AnticheatAPI.unexemptPlayer(player, CheckType.LONG_REACH);
					AnticheatAPI.unexemptPlayer(player, CheckType.NO_SWING);
				}
			} catch (IOException e1) {
				e1.printStackTrace();

				// unexempt player from AntiCheat check
				if (Bukkit.getServer().getPluginManager().getPlugin("AntiCheat") != null) {
					AnticheatAPI.unexemptPlayer(player, CheckType.FAST_PLACE);
					AnticheatAPI.unexemptPlayer(player, CheckType.FAST_BREAK);
					AnticheatAPI.unexemptPlayer(player, CheckType.LONG_REACH);
					AnticheatAPI.unexemptPlayer(player, CheckType.NO_SWING);
				}
			}

		} else
			return;
	}

	public static void circleChooser(Player player, World world, Block actBlock) {
		Block northBlock = actBlock.getRelative(0, 0, -1);
		Block southBlock = actBlock.getRelative(0, 0, 1);
		Block eastBlock = actBlock.getRelative(1, 0, 0);
		Block westBlock = actBlock.getRelative(-1, 0, 0);

		// teleportation circle
		if (northBlock.getType() == Material.REDSTONE_WIRE && southBlock.getType() == Material.REDSTONE_WIRE && eastBlock.getType() == Material.REDSTONE_WIRE && westBlock.getType() == Material.REDSTONE_WIRE) {
			if (player.hasPermission("geometricmagic.teleportation")) {
				// System.out.println("teleportation");
				teleportationCircle(player, world, actBlock);
			} else {
				player.sendMessage("You do not have permission to use this circle");
			}

			// micro circle
		} else if (northBlock.getType() != Material.REDSTONE_WIRE && southBlock.getType() != Material.REDSTONE_WIRE && eastBlock.getType() != Material.REDSTONE_WIRE && westBlock.getType() != Material.REDSTONE_WIRE && actBlock.getRelative(-3, 0, 0).getType() != Material.REDSTONE_WIRE && actBlock.getRelative(3, 0, 0).getType() != Material.REDSTONE_WIRE && actBlock.getRelative(0, 0, -3).getType() != Material.REDSTONE_WIRE && actBlock.getRelative(0, 0, 3).getType() != Material.REDSTONE_WIRE) {
			if (player.hasPermission("geometricmagic.micro")) {
				// System.out.println("micro");
				microCircle(player, world, actBlock);
			} else {
				player.sendMessage("You do not have permission to use this circle");
			}

			// transmutation circle
		} else if ((northBlock.getType() == Material.REDSTONE_WIRE && southBlock.getType() == Material.REDSTONE_WIRE && eastBlock.getType() != Material.REDSTONE_WIRE && westBlock.getType() != Material.REDSTONE_WIRE) || (northBlock.getType() != Material.REDSTONE_WIRE && southBlock.getType() != Material.REDSTONE_WIRE && eastBlock.getType() == Material.REDSTONE_WIRE && westBlock.getType() == Material.REDSTONE_WIRE)) {

			// transmutation circle size permissions
			// - allows use of all circles smaller than then the max
			// size permission node they have
			int transmutationCircleSize = 0;
			if (player.hasPermission("geometricmagic.transmutation.9")) {
				transmutationCircleSize = 9;
			} else if (player.hasPermission("geometricmagic.transmutation.7")) {
				transmutationCircleSize = 7;
			} else if (player.hasPermission("geometricmagic.transmutation.5")) {
				transmutationCircleSize = 5;
			} else if (player.hasPermission("geometricmagic.transmutation.3")) {
				transmutationCircleSize = 3;
			} else if (player.hasPermission("geometricmagic.transmutation.1")) {
				transmutationCircleSize = 1;
			} else {
				transmutationCircleSize = 0;
			}

			// Storage circle size permissions
			int storageCircleSize = 0;
			if (player.hasPermission("geometricmagic.storage.9")) {
				storageCircleSize = 9;
			} else if (player.hasPermission("geometricmagic.storage.7")) {
				storageCircleSize = 7;
			} else if (player.hasPermission("geometricmagic.storage.5")) {
				storageCircleSize = 5;
			} else if (player.hasPermission("geometricmagic.storage.3")) {
				storageCircleSize = 3;
			} else if (player.hasPermission("geometricmagic.storage.1")) {
				storageCircleSize = 1;
			} else {
				storageCircleSize = 0;
			}

			int circleSize = (transmutationCircleSize > storageCircleSize) ? transmutationCircleSize : storageCircleSize;

			// System.out.println("circleSize:" + circleSize);
			// System.out.println("transmutationCircleSize:" + transmutationCircleSize);
			// System.out.println("storageCircleSize:" + storageCircleSize);

			// transmute cool down
			/*
			 * if (!player.hasPermission("geometricmagic.bypass.cooldown")) { int coolDown = plugin.getConfig().getInt("transmutation.cooldown"); if (mapCoolDowns.containsKey(player.getName() +
			 * " transmute circle")) { long diff = (System.currentTimeMillis() - mapCoolDowns.get(player.getName() + " transmute circle")) / 1000; if (diff < coolDown) { // still cooling down
			 * player.sendMessage(coolDown - diff + " seconds before you can do that again."); return; } } mapCoolDowns.put(player.getName() + " transmute circle", System.currentTimeMillis()); }
			 */
			if (circleSize > 0) {
				transmutationCircle(player, world, actBlock, transmutationCircleSize, storageCircleSize);
			} else {
				player.sendMessage("You do not have permission to use this circle");
			}

			// set circle
		} else if (northBlock.getType() != Material.REDSTONE_WIRE && southBlock.getType() != Material.REDSTONE_WIRE && eastBlock.getType() != Material.REDSTONE_WIRE && westBlock.getType() != Material.REDSTONE_WIRE && actBlock.getRelative(-3, 0, 0).getType() == Material.REDSTONE_WIRE && actBlock.getRelative(3, 0, 0).getType() == Material.REDSTONE_WIRE && actBlock.getRelative(0, 0, -3).getType() == Material.REDSTONE_WIRE && actBlock.getRelative(0, 0, 3).getType() == Material.REDSTONE_WIRE) {

			if (player.hasPermission("geometricmagic.set")) {

				// exempt player from AntiCheat check
				if (Bukkit.getServer().getPluginManager().getPlugin("AntiCheat") != null) {
					AnticheatAPI.exemptPlayer(player, CheckType.FAST_PLACE);
					AnticheatAPI.exemptPlayer(player, CheckType.FAST_BREAK);
					AnticheatAPI.exemptPlayer(player, CheckType.LONG_REACH);
					AnticheatAPI.exemptPlayer(player, CheckType.NO_SWING);
				}
				
				// System.out.println("remote circle");

				setCircleRemote(player, world, actBlock);

				// unexempt player from AntiCheat check
				if (Bukkit.getServer().getPluginManager().getPlugin("AntiCheat") != null) {
					AnticheatAPI.unexemptPlayer(player, CheckType.FAST_PLACE);
					AnticheatAPI.unexemptPlayer(player, CheckType.FAST_BREAK);
					AnticheatAPI.unexemptPlayer(player, CheckType.LONG_REACH);
					AnticheatAPI.unexemptPlayer(player, CheckType.NO_SWING);
				}
			} else {
				player.sendMessage("You do not have permission to use this circle");
			}

			// no circle
		}
	}

	public static void teleportationCircle(Player player, World world, Block actBlock) {

		// activation block in center
		Location actPoint = actBlock.getLocation();

		// init some variables
		int na = 0, nb = 0, ea = 0, eb = 0, sa = 0, sb = 0, wa = 0, wb = 0, nc = 0, ec = 0, sc = 0, wc = 0;

		// set core blocks to air
		actBlock.setType(Material.AIR);
		actBlock.getRelative(1, 0, 0).setType(Material.AIR);
		actBlock.getRelative(-1, 0, 0).setType(Material.AIR);
		actBlock.getRelative(0, 0, 1).setType(Material.AIR);
		actBlock.getRelative(0, 0, -1).setType(Material.AIR);

		// count and track all redstone blocks
		Block curBlock = actBlock.getRelative(0, 0, -1);
		while (curBlock.getRelative(0, 0, -1).getType() == Material.REDSTONE_WIRE) {
			na++;
			curBlock = curBlock.getRelative(0, 0, -1);
		}
		Block fineBlock = curBlock;
		while (fineBlock.getRelative(-1, 0, 0).getType() == Material.REDSTONE_WIRE) {
			nb++;
			fineBlock = fineBlock.getRelative(-1, 0, 0);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(1, 0, 0).getType() == Material.REDSTONE_WIRE) {
			nc++;
			fineBlock = fineBlock.getRelative(1, 0, 0);
		}

		curBlock = actBlock.getRelative(1, 0, 0);
		while (curBlock.getRelative(1, 0, 0).getType() == Material.REDSTONE_WIRE) {
			ea++;
			curBlock = curBlock.getRelative(1, 0, 0);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(0, 0, -1).getType() == Material.REDSTONE_WIRE) {
			eb++;
			fineBlock = fineBlock.getRelative(0, 0, -1);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(0, 0, 1).getType() == Material.REDSTONE_WIRE) {
			ec++;
			fineBlock = fineBlock.getRelative(0, 0, 1);
		}

		curBlock = actBlock.getRelative(0, 0, 1);
		while (curBlock.getRelative(0, 0, 1).getType() == Material.REDSTONE_WIRE) {
			sa++;
			curBlock = curBlock.getRelative(0, 0, 1);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(1, 0, 0).getType() == Material.REDSTONE_WIRE) {
			sb++;
			fineBlock = fineBlock.getRelative(1, 0, 0);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(-1, 0, 0).getType() == Material.REDSTONE_WIRE) {
			sc++;
			fineBlock = fineBlock.getRelative(-1, 0, 0);
		}

		curBlock = actBlock.getRelative(-1, 0, 0);
		while (curBlock.getRelative(-1, 0, 0).getType() == Material.REDSTONE_WIRE) {
			wa++;
			curBlock = curBlock.getRelative(-1, 0, 0);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(0, 0, 1).getType() == Material.REDSTONE_WIRE) {
			wb++;
			fineBlock = fineBlock.getRelative(0, 0, 1);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(0, 0, -1).getType() == Material.REDSTONE_WIRE) {
			wc++;
			fineBlock = fineBlock.getRelative(0, 0, -1);
		}

		// set all redstone to air
		curBlock = actBlock.getRelative(0, 0, -1);
		for (int c = 0; c < na; c++) {
			curBlock = curBlock.getRelative(0, 0, -1);
			curBlock.setType(Material.AIR);
		}
		fineBlock = curBlock;
		for (int c = 0; c < nb; c++) {
			fineBlock = fineBlock.getRelative(-1, 0, 0);
			fineBlock.setType(Material.AIR);
		}
		fineBlock = curBlock;
		for (int c = 0; c < nc; c++) {
			fineBlock = fineBlock.getRelative(1, 0, 0);
			fineBlock.setType(Material.AIR);
		}

		curBlock = actBlock.getRelative(1, 0, 0);
		for (int c = 0; c < ea; c++) {
			curBlock = curBlock.getRelative(1, 0, 0);
			curBlock.setType(Material.AIR);
		}
		fineBlock = curBlock;
		for (int c = 0; c < eb; c++) {
			fineBlock = fineBlock.getRelative(0, 0, -1);
			fineBlock.setType(Material.AIR);
		}
		fineBlock = curBlock;
		for (int c = 0; c < ec; c++) {
			fineBlock = fineBlock.getRelative(0, 0, 1);
			fineBlock.setType(Material.AIR);
		}

		curBlock = actBlock.getRelative(0, 0, 1);
		for (int c = 0; c < sa; c++) {
			curBlock = curBlock.getRelative(0, 0, 1);
			curBlock.setType(Material.AIR);
		}
		fineBlock = curBlock;
		for (int c = 0; c < sb; c++) {
			fineBlock = fineBlock.getRelative(1, 0, 0);
			fineBlock.setType(Material.AIR);
		}
		fineBlock = curBlock;
		for (int c = 0; c < sc; c++) {
			fineBlock = fineBlock.getRelative(-1, 0, 0);
			fineBlock.setType(Material.AIR);
		}

		curBlock = actBlock.getRelative(-1, 0, 0);
		for (int c = 0; c < wa; c++) {
			curBlock = curBlock.getRelative(-1, 0, 0);
			curBlock.setType(Material.AIR);
		}
		fineBlock = curBlock;
		for (int c = 0; c < wb; c++) {
			fineBlock = fineBlock.getRelative(0, 0, 1);
			fineBlock.setType(Material.AIR);
		}
		fineBlock = curBlock;
		for (int c = 0; c < wc; c++) {
			fineBlock = fineBlock.getRelative(0, 0, -1);
			fineBlock.setType(Material.AIR);
		}

		// find out teleport location and modify it

		// north negative z
		// south positive z
		// east positive x
		// west negative x
		int z = ((sa * 100 + sb) - (na * 100 + nb));
		int x = ((ea * 100 + eb) - (wa * 100 + wb));
		int y = nc + ec + sc + wc;

		double actPointX = actPoint.getX();
		double actPointZ = actPoint.getZ();

		Location teleLoc = actPoint.add(x, y, z);

		float yaw = player.getLocation().getYaw();
		float pitch = player.getLocation().getPitch();

		teleLoc.setYaw(yaw);
		teleLoc.setPitch(pitch);

		double distance = Math.sqrt(Math.pow(teleLoc.getX() - actPointX, 2) + Math.pow(teleLoc.getZ() - actPointZ, 2));

		double mathRandX = philosopherStoneModifier(player) * distance / 10 * Math.random();
		double mathRandZ = philosopherStoneModifier(player) * distance / 10 * Math.random();

		double randX = (teleLoc.getX() - (0.5 * mathRandX)) + (mathRandX);
		double randZ = (teleLoc.getZ() - (0.5 * mathRandZ)) + (mathRandZ);

		teleLoc.setX(randX);
		teleLoc.setZ(randZ);

		// wait for chunk to be loaded before teleporting player
		// while (teleLoc.getWorld().getChunkAt(teleLoc).isLoaded() == false) {
		// teleLoc.getWorld().getChunkAt(teleLoc).load(true);
		// }
		// hopefully a bit more efficient
		//Chunk chunk = teleLoc.getChunk();
		//while (!chunk.isLoaded() || !chunk.load(true));

		// teleport player
		player.teleport(teleLoc);

		ItemStack redstonePile = new ItemStack(331, 5 + na + nb + nc + sa + sb + sc + ea + eb + ec + wa + wb + wc);

		teleLoc.getWorld().dropItem(teleLoc, redstonePile);

		doEffect(actBlock.getLocation());
		doEffect(teleLoc);
	}

	public static void microCircle(Player player, World world, Block actBlock) {
		if (getTransmutationCostSystem(plugin).equalsIgnoreCase("vault")) {
			Economy econ = GeometricMagic.getEconomy();

			// Tell the player how much money they have
			player.sendMessage("You have " + econ.format(getBalance(player)));
		} else if (getTransmutationCostSystem(plugin).equalsIgnoreCase("xp")) {
			// Tell the player how many levels they have
			player.sendMessage("Your experience level is " + player.getLevel());
		}

		// Tell player the set circles that are on cooldown
		int coolDown = plugin.getConfig().getInt("setcircles.cooldown");
		if (player.hasPermission("geometricmagic.set")) {
			int onCDCount = 0;
			for (String setCircleId : setCircleArray.keySet()) {
				long coolDownValue = getSetCircleCooldown(player, setCircleId);
				if (coolDownValue > 0) {
					onCDCount++;
					// still cooling down
					player.sendMessage(ChatColor.RED + "" + coolDownValue + " seconds before you can use set circle " + setCircleId + " (" + setCircleArray.get(setCircleId) + ")");
				}
			}
			if (onCDCount == 0) {
				player.sendMessage(ChatColor.GREEN + "Your set circles are all ready to use.");
			} else {
				player.sendMessage(ChatColor.GREEN + "Any remaining set circles are ready to use.");
			}
			long coolDownValue = getSetCircleCooldown(player, "learn");
			if (coolDownValue > 0) {
				player.sendMessage(ChatColor.RED + "" + coolDownValue + " seconds before you can learn another set circle.");
			}
		}

		// Tell player when they can use a transmute circle
		if (canTransmute(player)) {
			coolDown = plugin.getConfig().getInt("transmutation.cooldown");
			if (mapCoolDowns.containsKey(player.getName() + " transmute circle")) {
				long diff = (System.currentTimeMillis() - mapCoolDowns.get(player.getName() + " transmute circle")) / 1000;
				if (diff < coolDown) {
					// still cooling down
					long transmuteCD = coolDown - diff;
					player.sendMessage(ChatColor.RED + "" + transmuteCD + " seconds before you can use a transmutation circle.");
				} else {
					// off cooldown
					player.sendMessage(ChatColor.GREEN + "Your transmutation circle is ready to use.");
				}
			} else {
				// off cooldown
				player.sendMessage(ChatColor.GREEN + "Your transmutation circle is ready to use.");
			}
		}

		// Tell player when they can use a storage circle
		if (canUseStorage(player)) {
			coolDown = plugin.getConfig().getInt("storage.cooldown");

			// check storage availability
			String storageAvailability = "";
			List<File> files = new ArrayList<File>();
			files.add(new File("plugins/GeometricMagic/storage/" + player.getName() + ".1"));
			files.add(new File("plugins/GeometricMagic/storage/" + player.getName() + ".3"));
			files.add(new File("plugins/GeometricMagic/storage/" + player.getName() + ".5"));
			files.add(new File("plugins/GeometricMagic/storage/" + player.getName() + ".7"));
			files.add(new File("plugins/GeometricMagic/storage/" + player.getName() + ".9"));
			for (File file : files) {
				String name = file.getName();
				String size = name.substring(name.length() - 1);
				if(file.exists()) {
					storageAvailability += ChatColor.RED + "[" + size + "]";
				} else {
					storageAvailability += ChatColor.GREEN + "[" + size + "]";
				}
			}

			if (mapCoolDowns.containsKey(player.getName() + " storage circle")) {
				long diff = (System.currentTimeMillis() - mapCoolDowns.get(player.getName() + " storage circle")) / 1000;
				if (diff < coolDown) {
					// still cooling down
					long storageCD = coolDown - diff;
					player.sendMessage(ChatColor.RED + "" + storageCD + " seconds before you can use a storage circle.");
				} else {
					// off cooldown
					player.sendMessage(ChatColor.GREEN + "Your storage circle is ready to use.");
				}
			} else {
				// off cooldown
				player.sendMessage(ChatColor.GREEN + "Your storage circle is ready to use.");
			}

			player.sendMessage("Storage Availability: " + storageAvailability);
		}

		List<Entity> entitiesList = player.getNearbyEntities(100, 32, 100);
		int limitCount = plugin.getConfig().getInt("setcircles.limitarrows");

		for (int i = 0; i < entitiesList.size() && limitCount != 0; i++) {
			if (entitiesList.get(i) instanceof Arrow) {
				Arrow shotArrow = (Arrow) entitiesList.get(i);

				// only player arrows matter (no skeletons)
				if (!(shotArrow.getShooter() instanceof Player)) {
					continue;
				}

				if (shotArrow.getLocation().getBlock().getType() == Material.REDSTONE_WIRE) {
					limitCount--;
					Block newActPoint = shotArrow.getLocation().getBlock();
					Player newPlayer = (Player) shotArrow.getShooter();
					circleChooser(newPlayer, world, newActPoint);
				}
			}
		}
	}

	public static boolean canTransmute(Player player) {
		if (player.hasPermission("geometricmagic.transmutation.*") || player.hasPermission("geometricmagic.transmutation.1") || player.hasPermission("geometricmagic.transmutation.3") || player.hasPermission("geometricmagic.transmutation.5") || player.hasPermission("geometricmagic.transmutation.7") || player.hasPermission("geometricmagic.transmutation.9")) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean canUseStorage(Player player) {
		if (player.hasPermission("geometricmagic.storage.*") || player.hasPermission("geometricmagic.storage.1") || player.hasPermission("geometricmagic.storage.3") || player.hasPermission("geometricmagic.storage.5") || player.hasPermission("geometricmagic.storage.7") || player.hasPermission("geometricmagic.storage.9")) {
			return true;
		} else {
			return false;
		}
	}

	public static void transmutationCircle(Player player, World world, Block actBlock, int transmutationCircleSize, int storageCircleSize) {
		int halfWidth = 0;
		int fullWidth = 0;
		Location startLoc = actBlock.getLocation();
		Location endLoc = actBlock.getLocation();
		Location circleStart = actBlock.getLocation();
		Location circleEnd = actBlock.getLocation();
		Material fromType = actBlock.getType();
		Material toType = actBlock.getType();
		boolean doEffect = false;
		if (actBlock.getRelative(0, 0, -1).getType() == Material.REDSTONE_WIRE && actBlock.getRelative(0, 0, 1).getType() == Material.REDSTONE_WIRE) {
			halfWidth = 0;
			while (actBlock.getRelative(0, 0, -1 * halfWidth).getType() == Material.REDSTONE_WIRE) {
				halfWidth++;
			}
			fullWidth = (halfWidth * 2) - 1;
			int dimensionOfEffect = (fullWidth - 2) * (fullWidth - 2);
			if (actBlock.getRelative((fullWidth - 1), 0, 0).getType() == Material.REDSTONE_WIRE) {
				// east
				if (actBlock.getRelative((halfWidth - 1), 0, halfWidth).getType() == Material.REDSTONE_WIRE && actBlock.getRelative((halfWidth - 1), 0, (-1 * halfWidth)).getType() == Material.REDSTONE_WIRE) {

					// transmute cool down
					if (!player.hasPermission("geometricmagic.bypass.cooldown")) {
						int coolDown = plugin.getConfig().getInt("transmutation.cooldown");
						if (mapCoolDowns.containsKey(player.getName() + " transmute circle")) {
							long diff = (System.currentTimeMillis() - mapCoolDowns.get(player.getName() + " transmute circle")) / 1000;
							if (diff < coolDown) {
								// still cooling down
								player.sendMessage(coolDown - diff + " seconds before you can do that again.");
								return;
							}
						}
						mapCoolDowns.put(player.getName() + " transmute circle", System.currentTimeMillis());
					}

					if (fullWidth - 2 > transmutationCircleSize) {
						return;
					}
					Block fromBlock = actBlock.getLocation().add(halfWidth - 1, 0, -1 * (halfWidth + 1)).getBlock();
					Block toBlock = actBlock.getLocation().add(halfWidth - 1, 0, halfWidth + 1).getBlock();
					fromType = fromBlock.getType();
					toType = toBlock.getType();
					byte fromData = fromBlock.getData();
					byte toData = toBlock.getData();
					startLoc = actBlock.getLocation().add(fullWidth, 0, -1 * dimensionOfEffect / 2);
					// System.out.println(startLoc);
					endLoc = actBlock.getLocation().add(fullWidth + dimensionOfEffect - 1, dimensionOfEffect - 1, dimensionOfEffect / 2 - 1);
					// System.out.println(endLoc);
					circleStart = actBlock.getLocation().add(1, 0, -1 * (halfWidth - 2));
					// System.out.println(circleStart);
					circleEnd = actBlock.getLocation().add(fullWidth - 2, fullWidth - 3, halfWidth - 2);
					// System.out.println(circleEnd);
					alchemyCheck(fromType, fromData, toType, toData, circleStart, circleEnd, startLoc, endLoc, player, fullWidth - 2);
					doEffect = true;
				}
				// Storage circle
				else {
					if (!canUseStorage(player))
						return;

					// storage cool down
					if (!player.hasPermission("geometricmagic.bypass.cooldown")) {
						int coolDown = plugin.getConfig().getInt("storage.cooldown");
						if (mapCoolDowns.containsKey(player.getName() + " storage circle")) {
							long diff = (System.currentTimeMillis() - mapCoolDowns.get(player.getName() + " storage circle")) / 1000;
							if (diff < coolDown) {
								// still cooling down
								player.sendMessage(coolDown - diff + " seconds before you can do that again.");
								return;
							}
						}
						mapCoolDowns.put(player.getName() + " storage circle", System.currentTimeMillis());
					}

					if (fullWidth - 2 > storageCircleSize) {
						return;
					}
					startLoc = actBlock.getLocation().add(1, 0, (-1 * (halfWidth - 2)));
					endLoc = actBlock.getLocation().add((fullWidth - 2), (fullWidth - 3), (halfWidth - 2));
					storageCircle(startLoc, endLoc, player, (fullWidth - 2));
					doEffect = true;
				}
			} else if (actBlock.getRelative(-1 * (fullWidth - 1), 0, 0).getType() == Material.REDSTONE_WIRE) {
				// west
				// System.out.println("transmutationCircle west");
				if (actBlock.getRelative((-1 * (halfWidth - 1)), 0, halfWidth).getType() == Material.REDSTONE_WIRE && actBlock.getRelative((-1 * (halfWidth - 1)), 0, (-1 * halfWidth)).getType() == Material.REDSTONE_WIRE) {

					// transmute cool down
					if (!player.hasPermission("geometricmagic.bypass.cooldown")) {
						int coolDown = plugin.getConfig().getInt("transmutation.cooldown");
						if (mapCoolDowns.containsKey(player.getName() + " transmute circle")) {
							long diff = (System.currentTimeMillis() - mapCoolDowns.get(player.getName() + " transmute circle")) / 1000;
							if (diff < coolDown) {
								// still cooling down
								player.sendMessage(coolDown - diff + " seconds before you can do that again.");
								return;
							}
						}
						mapCoolDowns.put(player.getName() + " transmute circle", System.currentTimeMillis());
					}

					if (fullWidth - 2 > transmutationCircleSize) {
						return;
					}
					Block fromBlock = actBlock.getLocation().add(-1 * (halfWidth - 1), 0, halfWidth + 1).getBlock();
					Block toBlock = actBlock.getLocation().add((-1) * (halfWidth - 1), 0, (-1) * (halfWidth + 1)).getBlock();
					fromType = fromBlock.getType();
					toType = toBlock.getType();
					byte fromData = fromBlock.getData();
					byte toData = toBlock.getData();
					startLoc = actBlock.getLocation().add(-1 * fullWidth, 0, dimensionOfEffect / 2);
					endLoc = actBlock.getLocation().add(-1 * (fullWidth + dimensionOfEffect) + 1, dimensionOfEffect - 1, -1 * dimensionOfEffect / 2 + 1);
					circleStart = actBlock.getLocation().add(-1, 0, (halfWidth - 2));
					circleEnd = actBlock.getLocation().add(-1 * (fullWidth - 2), fullWidth - 3, -1 * (halfWidth - 2));
					alchemyCheck(fromType, fromData, toType, toData, circleStart, circleEnd, startLoc, endLoc, player, fullWidth - 2);
					doEffect = true;
				}
				// Storage circle
				else {
					if (!canUseStorage(player))
						return;

					// storage cool down
					if (!player.hasPermission("geometricmagic.bypass.cooldown")) {
						int coolDown = plugin.getConfig().getInt("storage.cooldown");
						if (mapCoolDowns.containsKey(player.getName() + " storage circle")) {
							long diff = (System.currentTimeMillis() - mapCoolDowns.get(player.getName() + " storage circle")) / 1000;
							if (diff < coolDown) {
								// still cooling down
								player.sendMessage(coolDown - diff + " seconds before you can do that again.");
								return;
							}
						}
						mapCoolDowns.put(player.getName() + " storage circle", System.currentTimeMillis());
					}

					if (fullWidth - 2 > storageCircleSize) {
						return;
					}
					startLoc = actBlock.getLocation().add(-1, 0, (halfWidth - 2));
					endLoc = actBlock.getLocation().add((-1 * (fullWidth - 2)), (fullWidth - 3), (-1 * (halfWidth - 2)));
					storageCircle(startLoc, endLoc, player, (fullWidth - 2));
					doEffect = true;
				}
			}
		} else if (actBlock.getRelative(1, 0, 0).getType() == Material.REDSTONE_WIRE && actBlock.getRelative(-1, 0, 0).getType() == Material.REDSTONE_WIRE) {
			halfWidth = 0;
			while (actBlock.getRelative(halfWidth, 0, 0).getType() == Material.REDSTONE_WIRE) {
				halfWidth++;
			}
			fullWidth = (halfWidth * 2) - 1;
			// System.out
			// .println("half is " + halfWidth + " full is " + fullWidth);
			int dimensionOfEffect = (fullWidth - 2) * (fullWidth - 2);
			if (actBlock.getRelative(0, 0, -1 * (fullWidth - 1)).getType() == Material.REDSTONE_WIRE) {
				// north
				// System.out.println("transmutationCircle north");
				if (actBlock.getRelative(halfWidth, 0, (-1 * (halfWidth - 1))).getType() == Material.REDSTONE_WIRE && actBlock.getRelative((-1 * halfWidth), 0, (-1 * (halfWidth - 1))).getType() == Material.REDSTONE_WIRE) {

					// transmute cool down
					if (!player.hasPermission("geometricmagic.bypass.cooldown")) {
						int coolDown = plugin.getConfig().getInt("transmutation.cooldown");
						if (mapCoolDowns.containsKey(player.getName() + " transmute circle")) {
							long diff = (System.currentTimeMillis() - mapCoolDowns.get(player.getName() + " transmute circle")) / 1000;
							if (diff < coolDown) {
								// still cooling down
								player.sendMessage(coolDown - diff + " seconds before you can do that again.");
								return;
							}
						}
						mapCoolDowns.put(player.getName() + " transmute circle", System.currentTimeMillis());
					}

					if (fullWidth - 2 > transmutationCircleSize) {
						return;
					}
					Block fromBlock = actBlock.getLocation().add(-1 * (halfWidth + 1), 0, -1 * (halfWidth - 1)).getBlock();
					Block toBlock = actBlock.getLocation().add(halfWidth + 1, 0, -1 * (halfWidth - 1)).getBlock();
					fromType = fromBlock.getType();
					toType = toBlock.getType();
					byte fromData = fromBlock.getData();
					byte toData = toBlock.getData();
					startLoc = actBlock.getLocation().add(-1 * dimensionOfEffect / 2, 0, -1 * fullWidth);
					endLoc = actBlock.getLocation().add(dimensionOfEffect / 2 - 1, dimensionOfEffect - 1, -1 * (dimensionOfEffect + fullWidth) + 1);
					circleStart = actBlock.getLocation().add(-1 * (halfWidth - 2), 0, -1);
					circleEnd = actBlock.getLocation().add((halfWidth - 2), fullWidth - 3, -1 * (fullWidth - 2));
					alchemyCheck(fromType, fromData, toType, toData, circleStart, circleEnd, startLoc, endLoc, player, fullWidth - 2);
					doEffect = true;
				}
				// Storage circle
				else {
					if (!canUseStorage(player))
						return;

					// storage cool down
					if (!player.hasPermission("geometricmagic.bypass.cooldown")) {
						int coolDown = plugin.getConfig().getInt("storage.cooldown");
						if (mapCoolDowns.containsKey(player.getName() + " storage circle")) {
							long diff = (System.currentTimeMillis() - mapCoolDowns.get(player.getName() + " storage circle")) / 1000;
							if (diff < coolDown) {
								// still cooling down
								player.sendMessage(coolDown - diff + " seconds before you can do that again.");
								return;
							}
						}
						mapCoolDowns.put(player.getName() + " storage circle", System.currentTimeMillis());
					}

					if (fullWidth - 2 > storageCircleSize) {
						return;
					}
					startLoc = actBlock.getLocation().add((-1 * (halfWidth - 2)), 0, -1);
					endLoc = actBlock.getLocation().add((halfWidth - 2), (fullWidth - 3), (-1 * (fullWidth - 2)));
					storageCircle(startLoc, endLoc, player, (fullWidth - 2));
					doEffect = true;
				}
			} else if (actBlock.getRelative(0, 0, (fullWidth - 1)).getType() == Material.REDSTONE_WIRE) {
				// south
				// System.out.println("transmutationCircle south");
				if (actBlock.getRelative(halfWidth, 0, (halfWidth - 1)).getType() == Material.REDSTONE_WIRE && actBlock.getRelative((-1 * halfWidth), 0, (halfWidth - 1)).getType() == Material.REDSTONE_WIRE) {

					// transmute cool down
					if (!player.hasPermission("geometricmagic.bypass.cooldown")) {
						int coolDown = plugin.getConfig().getInt("transmutation.cooldown");
						if (mapCoolDowns.containsKey(player.getName() + " transmute circle")) {
							long diff = (System.currentTimeMillis() - mapCoolDowns.get(player.getName() + " transmute circle")) / 1000;
							if (diff < coolDown) {
								// still cooling down
								player.sendMessage(coolDown - diff + " seconds before you can do that again.");
								return;
							}
						}
						mapCoolDowns.put(player.getName() + " transmute circle", System.currentTimeMillis());
					}

					if (fullWidth - 2 > transmutationCircleSize) {
						return;
					}
					Block fromBlock = actBlock.getLocation().add(halfWidth + 1, 0, halfWidth - 1).getBlock();
					Block toBlock = actBlock.getLocation().add(-1 * (halfWidth + 1), 0, halfWidth - 1).getBlock();
					fromType = fromBlock.getType();
					toType = toBlock.getType();
					byte fromData = fromBlock.getData();
					byte toData = toBlock.getData();
					startLoc = actBlock.getLocation().add(dimensionOfEffect / 2, 0, fullWidth);
					endLoc = actBlock.getLocation().add(-1 * dimensionOfEffect / 2 + 1, dimensionOfEffect - 1, fullWidth + dimensionOfEffect - 1);
					circleStart = actBlock.getLocation().add(halfWidth - 2, 0, 1);
					circleEnd = actBlock.getLocation().add(-1 * (halfWidth - 2), fullWidth - 3, (fullWidth - 2));
					alchemyCheck(fromType, fromData, toType, toData, circleStart, circleEnd, startLoc, endLoc, player, fullWidth - 2);
					doEffect = true;
				}
				// Storage circle
				else {
					if (!canUseStorage(player))
						return;

					// storage cool down
					if (!player.hasPermission("geometricmagic.bypass.cooldown")) {
						int coolDown = plugin.getConfig().getInt("storage.cooldown");
						if (mapCoolDowns.containsKey(player.getName() + " storage circle")) {
							long diff = (System.currentTimeMillis() - mapCoolDowns.get(player.getName() + " storage circle")) / 1000;
							if (diff < coolDown) {
								// still cooling down
								player.sendMessage(coolDown - diff + " seconds before you can do that again.");
								return;
							}
						}
						mapCoolDowns.put(player.getName() + " storage circle", System.currentTimeMillis());
					}

					if (fullWidth - 2 > storageCircleSize) {
						return;
					}
					startLoc = actBlock.getLocation().add((halfWidth - 2), 0, 1);
					endLoc = actBlock.getLocation().add((-1 * (halfWidth - 2)), (fullWidth - 3), (fullWidth - 2));
					storageCircle(startLoc, endLoc, player, (fullWidth - 2));
					doEffect = true;
				}
			}
		}
		if (doEffect)
			doEffect(actBlock.getLocation());
	}

	public static void setCircleRemote(Player player, World world, Block actBlock) {
		Boolean remote = false;
		Block effectBlock = actBlock;

		List<Entity> entitiesList = player.getNearbyEntities(100, 32, 100);
		int limit = plugin.getConfig().getInt("setcircles.limitarrows");
		int limitCount = limit;

		for (int i = 0; i < entitiesList.size() && limitCount != 0; i++) {
			if (entitiesList.get(i) instanceof Arrow) {
				Arrow shotArrow = (Arrow) entitiesList.get(i);
				if (shotArrow.getLocation().getBlock().getX() == actBlock.getLocation().getBlock().getX() && shotArrow.getLocation().getBlock().getZ() == actBlock.getLocation().getBlock().getZ()) {
					limitCount--;
					remote = true;
					entitiesList.remove(i);
				}
			}
		}

		limitCount = limit;

		if (remote) {
			for (int i = 0; i < entitiesList.size() && limitCount != 0; i++) {
				if (entitiesList.get(i) instanceof Arrow) {
					limitCount--;
					Arrow shotArrow = (Arrow) entitiesList.get(i);
					effectBlock = shotArrow.getLocation().getBlock();
					setCircle(player, world, actBlock, effectBlock);
				}
			}
		} else
			setCircle(player, world, actBlock, effectBlock);
	}

	public static void setCircle(Player player, World world, Block actBlock, Block effectBlock) {
		Block northSin = actBlock.getRelative(0, 0, -3);
		Block southSin = actBlock.getRelative(0, 0, 3);
		Block eastSin = actBlock.getRelative(3, 0, 0);
		Block westSin = actBlock.getRelative(-3, 0, 0);
		int n = 0;
		int s = 0;
		int e = 0;
		int w = 0;
		int[] intArray = new int[4];
		if (northSin.getRelative(BlockFace.NORTH).getType() == Material.REDSTONE_WIRE)
			n++;
		if (northSin.getRelative(BlockFace.SOUTH).getType() == Material.REDSTONE_WIRE)
			n++;
		if (northSin.getRelative(BlockFace.EAST).getType() == Material.REDSTONE_WIRE)
			n++;
		if (northSin.getRelative(BlockFace.WEST).getType() == Material.REDSTONE_WIRE)
			n++;
		intArray[0] = n;
		if (southSin.getRelative(BlockFace.NORTH).getType() == Material.REDSTONE_WIRE)
			s++;
		if (southSin.getRelative(BlockFace.SOUTH).getType() == Material.REDSTONE_WIRE)
			s++;
		if (southSin.getRelative(BlockFace.EAST).getType() == Material.REDSTONE_WIRE)
			s++;
		if (southSin.getRelative(BlockFace.WEST).getType() == Material.REDSTONE_WIRE)
			s++;
		intArray[1] = s;
		if (eastSin.getRelative(BlockFace.NORTH).getType() == Material.REDSTONE_WIRE)
			e++;
		if (eastSin.getRelative(BlockFace.SOUTH).getType() == Material.REDSTONE_WIRE)
			e++;
		if (eastSin.getRelative(BlockFace.EAST).getType() == Material.REDSTONE_WIRE)
			e++;
		if (eastSin.getRelative(BlockFace.WEST).getType() == Material.REDSTONE_WIRE)
			e++;
		intArray[2] = e;
		if (westSin.getRelative(BlockFace.NORTH).getType() == Material.REDSTONE_WIRE)
			w++;
		if (westSin.getRelative(BlockFace.SOUTH).getType() == Material.REDSTONE_WIRE)
			w++;
		if (westSin.getRelative(BlockFace.EAST).getType() == Material.REDSTONE_WIRE)
			w++;
		if (westSin.getRelative(BlockFace.WEST).getType() == Material.REDSTONE_WIRE)
			w++;
		intArray[3] = w;
		Arrays.sort(intArray);
		String arrayString = Arrays.toString(intArray);
		try {
			setCircleEffects(player, world, actBlock, effectBlock, arrayString);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	// deprecated
	public static long getSetCircleCooldown(Player player, String setCircle) {
		int coolDown = plugin.getConfig().getInt("setcircles." + setCircle + ".cooldown");
		String cdKey = player.getName() + " set circle " + setCircle;
		long retValue = 0;
		if (!player.hasPermission("geometricmagic.bypass.cooldown")) {
			if (mapCoolDowns.containsKey(cdKey)) {
				long diff = (System.currentTimeMillis() - mapCoolDowns.get(cdKey)) / 1000;
				retValue = coolDown - diff;
			}
		}
		// System.out.println("cooldown:" + retValue);
		return retValue;
	}

	// deprecated
	public static void setSetCircleCooldown(String playerName, String setCircleId, Long time) {
		mapCoolDowns.put(playerName + " set circle " + setCircleId, time);
	}

	public static long getCooldown(Player player, String path) {
		int coolDown = plugin.getConfig().getInt(path);
		String cdKey = player.getName() + " | " + path;
		long retValue = 0;
		if (!player.hasPermission("geometricmagic.bypass.cooldown")) {
			if (mapCoolDowns.containsKey(cdKey)) {
				long diff = (System.currentTimeMillis() - mapCoolDowns.get(cdKey)) / 1000;
				retValue = coolDown - diff;
			}
		}
		// System.out.println("cooldown:" + retValue);
		return retValue;
	}

	public static void setCooldown(String playerName, String path, Long time) {
		mapCoolDowns.put(playerName + " | " + path, time);
	}

	// TODO: fireworks spell and harness the ice spell
	public static void setCircleEffects(Player player, World world, Block actBlock, Block effectBlock, String arrayString) throws IOException {
		Location effectBlockLocation = effectBlock.getLocation();
		int cost = 0;
		if (!hasLearnedCircle(player, arrayString)) {
			long cdCheck = getSetCircleCooldown(player, "learn");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can learn another set circle.");
				return;
			} else if (learnCircle(player, arrayString, actBlock)) {
				player.sendMessage("You have successfully learned the circle " + arrayString);
				return;
			}
		}

		Boolean doEffect = false;

		// nothing
		if (arrayString.equals("0"))
			return;

		// Spawn weapon circle
		if (arrayString.equals("[1, 1, 1, 1]") && player.hasPermission("geometricmagic.set.1111")) {
			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			}

			long cdCheck = getSetCircleCooldown(player, "1111");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 1111 (" + setCircleArray.get("1111") + ") again.");
				return;
			}

			cost = plugin.getConfig().getInt("setcircles.1111.cost");
			if (cost > 20)
				cost = 20;

			if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {
				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}

				int itemID = plugin.getConfig().getInt("setcircles.1111.item");
				ItemStack item = new ItemStack(itemID);
				effectBlock.getWorld().dropItem(effectBlockLocation, item);
				setSetCircleCooldown(player.getName(), "1111", System.currentTimeMillis());

				doEffect = true;

			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Repair Circle
		else if (arrayString.equals("[1, 1, 3, 3]") && player.hasPermission("geometricmagic.set.1133")) {
			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			}

			long cdCheck = getSetCircleCooldown(player, "1133");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 1133 (" + setCircleArray.get("1133") + ") again.");
				return;
			}

			if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {
				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}
			}

			int count = 0;
			List<Entity> repairEntities = player.getNearbyEntities(9, 10, 9);
			for (int i = 0; i < repairEntities.size(); i++) {
				if (repairEntities.get(i) instanceof Item) {
					Item droppedItem = (Item) repairEntities.get(i);
					ItemStack droppedItemStack = droppedItem.getItemStack();

					// Item data value
					int itemCode = droppedItemStack.getTypeId();

					// Item dye
					/*
					 * Color itemDyeColor = null; if (Armor.isApplicable(droppedItemStack)) { itemDyeColor = Armor.getColor(droppedItemStack); System.out.println("First:" + itemDyeColor); }
					 * 
					 * // Enchantments Map<Enchantment, Integer> effects = droppedItemStack.getEnchantments();
					 */

					// Get cost
					if ((256 <= itemCode && itemCode <= 258) || (267 <= itemCode && itemCode <= 279) || (283 <= itemCode && itemCode <= 286) || (290 <= itemCode && itemCode <= 294) || (298 <= itemCode && itemCode <= 317) || itemCode == 259 || itemCode == 346 || itemCode == 359 || itemCode == 261) {
						if ((256 <= itemCode && itemCode <= 258) || itemCode == 267 || itemCode == 292)
							cost = droppedItemStack.getDurability();
						if ((268 <= itemCode && itemCode <= 271) || itemCode == 290)
							cost = droppedItemStack.getDurability();
						if ((272 <= itemCode && itemCode <= 275) || itemCode == 291)
							cost = droppedItemStack.getDurability();
						if ((276 <= itemCode && itemCode <= 279) || itemCode == 293)
							cost = droppedItemStack.getDurability();
						if ((283 <= itemCode && itemCode <= 286) || itemCode == 294)
							cost = droppedItemStack.getDurability();
						if (itemCode == 298)
							cost = droppedItemStack.getDurability();
						if (itemCode == 299)
							cost = droppedItemStack.getDurability();
						if (itemCode == 300)
							cost = droppedItemStack.getDurability();
						if (itemCode == 301)
							cost = droppedItemStack.getDurability();
						if (itemCode == 306)
							cost = droppedItemStack.getDurability();
						if (itemCode == 307)
							cost = droppedItemStack.getDurability();
						if (itemCode == 308)
							cost = droppedItemStack.getDurability();
						if (itemCode == 309)
							cost = droppedItemStack.getDurability();
						if (itemCode == 310)
							cost = droppedItemStack.getDurability();
						if (itemCode == 311)
							cost = droppedItemStack.getDurability();
						if (itemCode == 312)
							cost = droppedItemStack.getDurability();
						if (itemCode == 313)
							cost = droppedItemStack.getDurability();
						if (itemCode == 314)
							cost = droppedItemStack.getDurability();
						if (itemCode == 315)
							cost = droppedItemStack.getDurability();
						if (itemCode == 316)
							cost = droppedItemStack.getDurability();
						if (itemCode == 317)
							cost = droppedItemStack.getDurability();
						if (itemCode == 259)
							cost = droppedItemStack.getDurability();
						if (itemCode == 346)
							cost = droppedItemStack.getDurability();
						if (itemCode == 359)
							cost = droppedItemStack.getDurability();
						if (itemCode == 261)
							cost = droppedItemStack.getDurability();
						cost = cost / 50;

						// Make sure cost is not more than 20
						if (cost > 20)
							cost = 20;

						// Use proper materials to repair items
						// TODO use item like brewing stand to repair in
						boolean found = false;
						Material repairMat = null;
						for (int i1 = 0; i1 < repairEntities.size(); i1++) {

							if (repairEntities.get(i1) instanceof Item) {
								Item droppedItem1 = (Item) repairEntities.get(i1);
								ItemStack droppedItemStack1 = droppedItem1.getItemStack();
								repairMat = getRepairMaterial(droppedItemStack.getType());

								if (droppedItemStack1.getType() == repairMat) {
									int amount = droppedItemStack1.getAmount() - 1;


									// if player has enough hunger bar left
									if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {
										if (!player.hasPermission("geometricmagic.bypass.hunger")) {
											player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
										}

										if (amount == 0) {
											droppedItem1.remove();
										} else {
											droppedItemStack1.setAmount(amount);
										}

										ItemStack newItem = new ItemStack(itemCode, 1);
										short maxDurability = newItem.getDurability();

										droppedItemStack.setDurability(maxDurability);
										count++;

									} else {
										player.sendMessage("You feel so hungry...");
										if (count > 0) {
											doEffect(effectBlockLocation);
										}
										return;
									}
									
									found = true;
								}
							}
						}

						// if a material wasn't found for this item go to the next item
						if (!found) {
							continue;
						}

					}
				}
			}

			setSetCircleCooldown(player.getName(), "1133", System.currentTimeMillis());
			doEffect = true;

		}

		// Conversion Circle
		else if (arrayString.equals("[1, 2, 2, 2]") && player.hasPermission("geometricmagic.set.1222")) {

			cost = plugin.getConfig().getInt("setcircles.1222.cost");
			if (cost > 20)
				cost = 20;

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			}

			long cdCheck = getSetCircleCooldown(player, "1222");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 1222 (" + setCircleArray.get("1222") + ") again.");
				return;
			}

			if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {
				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}

				ItemStack oneRedstone = new ItemStack(331, 1);
				Item redStack = effectBlock.getWorld().dropItem(effectBlockLocation, oneRedstone);
				List<Entity> entityList = redStack.getNearbyEntities(5, 10, 5);
				for (int i = 0; i < entityList.size(); i++) {
					if (entityList.get(i) instanceof Item) {
						Item droppedItem = (Item) entityList.get(i);
						ItemStack droppedItemStack = droppedItem.getItemStack();

						// Skip items because they don't have values
						if (droppedItemStack.getTypeId() > 255) {
							player.sendMessage("You can't use items, only blocks");
							continue;
						}

						// check if player has permission to break blocks here first
						if (!checkBlockBreakSimulation(droppedItem.getLocation(), player)) {
							// player.sendMessage("You don't have permission to do that there.");
							redStack.remove();
							return;
						}

						double valueArray = getBlockValue(plugin, droppedItemStack.getTypeId(), droppedItemStack.getData().getData(), droppedItemStack.getDurability(), "sell", player);

						double pay = (valueArray * droppedItemStack.getAmount());

						if (getTransmutationCostSystem(plugin).equalsIgnoreCase("vault")) {
							Economy econ = GeometricMagic.getEconomy();
							if (pay > 0) {
								econ.depositPlayer(player.getName(), pay);
								player.sendMessage("Deposited " + pay + " " + econ.currencyNamePlural() + " for " + droppedItemStack.getType().name().toLowerCase().replace('_', ' '));
								droppedItem.remove();
							} else if (pay < 0) {
								econ.withdrawPlayer(player.getName(), pay * -1);
								player.sendMessage("Withdrew " + pay + " " + econ.currencyNamePlural() + " for " + droppedItemStack.getType().name().toLowerCase().replace('_', ' '));
								droppedItem.remove();
							}
						} else if (getTransmutationCostSystem(plugin).equalsIgnoreCase("xp")) {
							player.setLevel((int) ((valueArray * droppedItemStack.getAmount()) + player.getLevel()));
							droppedItem.remove();
						}

						/*
						 * player.setLevel((valueArray[droppedItemStack .getTypeId()] * droppedItemStack .getAmount()) + player.getLevel());
						 */
					}
				}
				redStack.remove();
				setSetCircleCooldown(player.getName(), "1222", System.currentTimeMillis());
				doEffect = true;

			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Philosopher's Stone Circle
		else if (arrayString.equals("[1, 2, 3, 3]") && player.hasPermission("geometricmagic.set.1233")) {

			cost = plugin.getConfig().getInt("setcircles.1233.cost");
			if (cost > 20)
				cost = 20;

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			}

			long cdCheck = getSetCircleCooldown(player, "1233");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 1233 (" + setCircleArray.get("1233") + ") again.");
				return;
			}

			ItemStack onePortal = new ItemStack(90, 1);
			int fires = 0;
			List<Entity> entityList = player.getNearbyEntities(10, 10, 10);
			for (int i = 0; i < entityList.size(); i++) {
				if (entityList.get(i) instanceof Item) {
					Item sacrifice = (Item) entityList.get(i);

					// check if player has permission to break blocks here first
					if (!checkBlockBreakSimulation(sacrifice.getLocation(), player)) {
						// player.sendMessage("You don't have permission to do that there.");
						return;
					}

					if (sacrifice.getItemStack().getType() == Material.FIRE) {
						fires += sacrifice.getItemStack().getAmount();
						sacrifice.remove();
					}
				}
			}
			if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {
				if (fires >= 64) {
					fires -= 64;
					if (!player.hasPermission("geometricmagic.bypass.hunger")) {
						player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
					}
					effectBlock.getWorld().dropItem(effectBlockLocation, onePortal);
					setSetCircleCooldown(player.getName(), "1233", System.currentTimeMillis());
					doEffect = true;
				}
			} else {
				player.sendMessage("You feel so hungry...");
			}
			// ItemStack diamondStack = new ItemStack(264, fires);
			// effectBlock.getWorld().dropItem(effectBlockLocation, diamondStack);
		}

		// Boron Circle
		else if (arrayString.equals("[1, 2, 3, 4]") && player.hasPermission("geometricmagic.set.1234")) {

			cost = plugin.getConfig().getInt("setcircles.1234.cost");
			if (cost > 20)
				cost = 20;

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			}

			long cdCheck = getSetCircleCooldown(player, "learn");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 1234 (" + setCircleArray.get("1234") + ") again.");
				return;
			}

			if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {
				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}
				player.sendMessage(ChatColor.GREEN + "The four elements, like man alone, are weak. But together they form the strong fifth element: boron -Brother Silence");
				int amount = plugin.getConfig().getInt("setcircles.1234.amount");
				ItemStack oneRedstone = new ItemStack(331, amount);

				effectBlock.getWorld().dropItem(effectBlockLocation, oneRedstone);
				setSetCircleCooldown(player.getName(), "1234", System.currentTimeMillis());
				doEffect = true;

			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Soul Circle
		else if (arrayString.equals("[2, 2, 2, 3]") && player.hasPermission("geometricmagic.set.2223")) {

			cost = plugin.getConfig().getInt("setcircles.2223.cost");
			if (cost > 20)
				cost = 20;

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			}

			long cdCheck = getSetCircleCooldown(player, "2223");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 2223 (" + setCircleArray.get("2223") + ") again.");
				return;
			}

			ItemStack oneRedstone = new ItemStack(331, 1);
			if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {
				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}
				Item redStack = effectBlock.getWorld().dropItem(effectBlockLocation, oneRedstone);
				int size = setCircleSize(actBlock);
				List<Entity> entityList = redStack.getNearbyEntities(size + 5, 128, size + 5);

				for (int i = 0; i < entityList.size(); i++) {
					Entity victimEntity = entityList.get(i);
					if (entityList.get(i) instanceof Player) {
						HumanEntity victim = (HumanEntity) victimEntity;
						Location victimLocation = victim.getLocation();

						if (!victim.equals(player)) {
							doEffect(victimLocation);
							if (victim.getInventory().contains(Material.FIRE)) {
								for (int k = 0; k < player.getInventory().getSize(); k++) {
									if (player.getInventory().getItem(i).getType() == Material.FIRE) {
										// System.out.println("removed a fire");
										int amount = player.getInventory().getItem(k).getAmount();
										player.getInventory().getItem(k).setAmount(amount - 1);
										if (amount - 1 <= 0) {
											player.getInventory().clear(k);
										}
									}
								}
							} else {
								EntityDamageEvent ede = new EntityDamageEvent(victimEntity, EntityDamageEvent.DamageCause.LIGHTNING, 1000);
								plugin.getServer().getPluginManager().callEvent(ede);
								if (ede.isCancelled())
									continue;

								ede.getEntity().setLastDamageCause(ede);
								victim.setHealth(0);
								// victim.damage(20);
							}
							if (victim.isDead()) {
								ItemStack oneFire = new ItemStack(51, 1);
								victim.getWorld().dropItem(actBlock.getLocation(), oneFire);
							}
							// only do one at a time
							doEffect = true;
							break;
						}
					}
					if (victimEntity instanceof Villager) {
						Villager victim = (Villager) victimEntity;
						doEffect(victim.getLocation());
						EntityDamageEvent ede = new EntityDamageEvent(victimEntity, EntityDamageEvent.DamageCause.LIGHTNING, 1000);
						plugin.getServer().getPluginManager().callEvent(ede);
						if (ede.isCancelled())
							continue;

						ede.getEntity().setLastDamageCause(ede);
						victim.setHealth(0);
						// victim.damage(20);
						if (victim.isDead()) {
							ItemStack oneFire = new ItemStack(51, 1);
							
							// TODO: add item name to items
							
							victim.getWorld().dropItem(actBlock.getLocation(), oneFire);
						}
						// only do one at a time
						break;
					}
				}
				redStack.remove();
				setSetCircleCooldown(player.getName(), "2223", System.currentTimeMillis());
				doEffect = true;
			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Homunculus Circle
		else if (arrayString.equals("[2, 2, 2, 4]") && player.hasPermission("geometricmagic.set.2224")) {

			cost = plugin.getConfig().getInt("setcircles.2224.cost");
			if (cost > 20)
				cost = 20;

			long cdCheck = getSetCircleCooldown(player, "2224");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 2224 (" + setCircleArray.get("2224") + ") again.");
				return;
			}

			if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {
				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}
				Location spawnLoc = effectBlockLocation;
				spawnLoc.add(0.5, 1, 0.5);
				effectBlock.getWorld().spawn(spawnLoc, Enderman.class);
				setSetCircleCooldown(player.getName(), "2224", System.currentTimeMillis());
				doEffect = true;
			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Safe Teleportation Circle
		else if (arrayString.equals("[2, 2, 4, 4]") && player.hasPermission("geometricmagic.set.2244")) {
			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			}

			cost = plugin.getConfig().getInt("setcircles.2244.cost");
			if (cost > 20)
				cost = 20;

			long cdCheck = getSetCircleCooldown(player, "2244");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 2244 (" + setCircleArray.get("2244") + ") again.");
				return;
			}

			Location actPoint = effectBlockLocation;
			int na = 0, nb = 0, ea = 0, eb = 0, sa = 0, sb = 0, wa = 0, wb = 0;
			Block curBlock = effectBlock.getRelative(0, 0, -1);
			while (curBlock.getRelative(0, 0, -1).getType() == Material.REDSTONE_WIRE) {
				na++;
				curBlock = curBlock.getRelative(0, 0, -1);
			}
			Block fineBlock = curBlock;
			while (fineBlock.getRelative(-1, 0, 0).getType() == Material.REDSTONE_WIRE) {
				nb++;
				fineBlock = fineBlock.getRelative(-1, 0, 0);
			}
			curBlock = effectBlock.getRelative(1, 0, 0);
			while (curBlock.getRelative(1, 0, 0).getType() == Material.REDSTONE_WIRE) {
				ea++;
				curBlock = curBlock.getRelative(1, 0, 0);
			}
			fineBlock = curBlock;
			while (fineBlock.getRelative(0, 0, -1).getType() == Material.REDSTONE_WIRE) {
				eb++;
				fineBlock = fineBlock.getRelative(0, 0, -1);
			}
			curBlock = effectBlock.getRelative(0, 0, 1);
			while (curBlock.getRelative(0, 0, 1).getType() == Material.REDSTONE_WIRE) {
				sa++;
				curBlock = curBlock.getRelative(0, 0, 1);
			}
			fineBlock = curBlock;
			while (fineBlock.getRelative(1, 0, 0).getType() == Material.REDSTONE_WIRE) {
				sb++;
				fineBlock = fineBlock.getRelative(1, 0, 0);
			}
			curBlock = effectBlock.getRelative(-1, 0, 0);
			while (curBlock.getRelative(-1, 0, 0).getType() == Material.REDSTONE_WIRE) {
				wa++;
				curBlock = curBlock.getRelative(-1, 0, 0);
			}
			fineBlock = curBlock;
			while (fineBlock.getRelative(0, 0, 1).getType() == Material.REDSTONE_WIRE) {
				wb++;
				fineBlock = fineBlock.getRelative(0, 0, 1);
			}
			// north negative z, south positive z, east positive x, west
			// negative x
			int z = ((sa * 100 + sb) - (na * 100 + nb));
			int x = ((ea * 100 + eb) - (wa * 100 + wb));
			double actPointX = actPoint.getX();
			double actPointZ = actPoint.getZ();
			Location teleLoc = actPoint.add(x, 0, z);
			double distance = Math.sqrt(Math.pow(teleLoc.getX() - actPointX, 2) + Math.pow(teleLoc.getZ() - actPointZ, 2));
			double mathRandX = philosopherStoneModifier(player) * distance / 10 * Math.random();
			double mathRandZ = philosopherStoneModifier(player) * distance / 10 * Math.random();
			double randX = (teleLoc.getX() - (0.5 * mathRandX)) + (mathRandX);
			double randZ = (teleLoc.getZ() - (0.5 * mathRandZ)) + (mathRandZ);
			teleLoc.setX(randX);
			teleLoc.setZ(randZ);
			while (teleLoc.getWorld().getChunkAt(teleLoc).isLoaded() == false) {
				teleLoc.getWorld().getChunkAt(teleLoc).load(true);
			}
			int highestBlock = teleLoc.getWorld().getHighestBlockYAt(teleLoc) + 1;
			// System.out.println( mathRandX + " " + mathRandZ );
			player.sendMessage("Safe teleportation altitude is at " + highestBlock);
			setSetCircleCooldown(player.getName(), "2244", System.currentTimeMillis());
			doEffect = true;
			return;
		}

		// Explosion Circle
		else if (arrayString.equals("[2, 3, 3, 3]") && player.hasPermission("geometricmagic.set.2333")) {

			cost = plugin.getConfig().getInt("setcircles.2333.cost");
			if (cost > 20)
				cost = 20;

			int size = setCircleSize(actBlock);
			cost = cost + size / 2;

			// Make sure cost is not more than 20
			if (cost > 20)
				cost = 20;

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			}

			long cdCheck = getSetCircleCooldown(player, "2333");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 2333 (" + setCircleArray.get("2333") + ") again.");
				return;
			}

			if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {
				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}

				// check if player has permission to break blocks here first
				if (!checkBlockBreakSimulation(effectBlockLocation, player)) {
					// player.sendMessage("You don't have permission to do that there.");
					return;
				}

				Fireball fireball = effectBlockLocation.getWorld().spawn(effectBlockLocation, Fireball.class);
				fireball.setIsIncendiary(false);
				fireball.setYield(4 + size);
				setSetCircleCooldown(player.getName(), "2333", System.currentTimeMillis());
				doEffect = true;
			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Fire Circle
		else if (arrayString.equals("[3, 3, 3, 4]") && player.hasPermission("geometricmagic.set.3334")) {

			cost = plugin.getConfig().getInt("setcircles.3334.cost");
			if (cost > 20)
				cost = 20;

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			}

			long cdCheck = getSetCircleCooldown(player, "3334");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 3334 (" + setCircleArray.get("3334") + ") again.");
				return;
			}

			if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {
				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}

				Integer circleSize = plugin.getConfig().getInt("setcircles.3334.size");

				alchemyFiller(Material.AIR, (byte) 0, Material.FIRE, (byte) 0, effectBlock.getRelative((circleSize / 2) * -1, 0, (circleSize / 2) * -1).getLocation(), effectBlock.getRelative(circleSize / 2, circleSize, circleSize / 2).getLocation(), player, false);

				setSetCircleCooldown(player.getName(), "3334", System.currentTimeMillis());
				doEffect = true;

			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Fire Explosion Circle
		else if (arrayString.equals("[3, 3, 4, 4]") && player.hasPermission("geometricmagic.set.3344")) {

			cost = plugin.getConfig().getInt("setcircles.3344.cost");
			if (cost > 20)
				cost = 20;

			int size = setCircleSize(actBlock);
			cost = cost + size / 2;

			// Make sure cost is not more than 20
			if (cost > 20)
				cost = 20;

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			}

			long cdCheck = getSetCircleCooldown(player, "3344");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 3344 (" + setCircleArray.get("3344") + ") again.");
				return;
			}

			if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {
				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}

				// check if player has permission to break blocks here first
				if (!checkBlockBreakSimulation(effectBlockLocation, player)) {
					// player.sendMessage("You don't have permission to do that there.");
					return;
				}

				Fireball fireball = effectBlockLocation.getWorld().spawn(effectBlockLocation, Fireball.class);
				fireball.setIsIncendiary(true);
				fireball.setYield(4 + size);
				setSetCircleCooldown(player.getName(), "3344", System.currentTimeMillis());
				doEffect = true;
			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Human Transmutation Circle
		else if (arrayString.equals("[3, 4, 4, 4]") && player.hasPermission("geometricmagic.set.3444")) {

			cost = plugin.getConfig().getInt("setcircles.3444.cost");
			if (cost > 20)
				cost = 20;

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			}

			long cdCheck = getSetCircleCooldown(player, "3444");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 3444 (" + setCircleArray.get("3444") + ") again.");
				return;
			}

			if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {
				try {
					if (!player.hasPermission("geometricmagic.bypass.hunger")) {
						player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
					}
					humanTransmutation(player);
					setSetCircleCooldown(player.getName(), "3444", System.currentTimeMillis());
					doEffect = true;
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Bed Circle
		else if (arrayString.equals("[0, 1, 1, 1]") && player.hasPermission("geometricmagic.set.0111")) {

			// using x111 because yml doesn't like 0 as first character
			cost = plugin.getConfig().getInt("setcircles.x111.cost");
			if (cost > 20)
				cost = 20;

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			} else if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {

				long cdCheck = getSetCircleCooldown(player, "x111");
				if (cdCheck > 0) {
					player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 0111 (" + setCircleArray.get("0111") + ") again.");
					return;
				}

				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}
				Location playerSpawn = player.getBedSpawnLocation();
				if (playerSpawn != null) {
					if (playerSpawn.getBlock().getType() == Material.AIR) {
						player.teleport(playerSpawn);
					} else {
						if (new Location(player.getWorld(), playerSpawn.getX() + 1, playerSpawn.getY(), playerSpawn.getZ()).getBlock().getType() == Material.AIR) {
							player.teleport(new Location(player.getWorld(), playerSpawn.getX() + 1, playerSpawn.getY(), playerSpawn.getZ()));
						} else if (new Location(player.getWorld(), playerSpawn.getX() - 1, playerSpawn.getY(), playerSpawn.getZ()).getBlock().getType() == Material.AIR) {
							player.teleport(new Location(player.getWorld(), playerSpawn.getX() - 1, playerSpawn.getY(), playerSpawn.getZ()));
						} else if (new Location(player.getWorld(), playerSpawn.getX(), playerSpawn.getY(), playerSpawn.getZ() + 1).getBlock().getType() == Material.AIR) {
							player.teleport(new Location(player.getWorld(), playerSpawn.getX(), playerSpawn.getY(), playerSpawn.getZ() + 1));
						} else if (new Location(player.getWorld(), playerSpawn.getX(), playerSpawn.getY(), playerSpawn.getZ() - 1).getBlock().getType() == Material.AIR) {
							player.teleport(new Location(player.getWorld(), playerSpawn.getX(), playerSpawn.getY(), playerSpawn.getZ() - 1));
						} else {
							player.sendMessage("Your bed is not safe to teleport to!");
							if (!player.hasPermission("geometricmagic.bypass.hunger")) {
								player.setFoodLevel((int) (player.getFoodLevel() + (cost * philosopherStoneModifier(player))));
							}
						}
					}
				} else {
					player.sendMessage("You do not have a spawn set!");
					if (!player.hasPermission("geometricmagic.bypass.hunger")) {
						player.setFoodLevel((int) (player.getFoodLevel() + (cost * philosopherStoneModifier(player))));
					}
				}
				setSetCircleCooldown(player.getName(), "x111", System.currentTimeMillis());
				doEffect = true;
			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Pig Circle
		else if (arrayString.equals("[0, 0, 4, 4]") && player.hasPermission("geometricmagic.set.0044")) {

			// using x044 because yml doesn't like 0 as first character
			cost = plugin.getConfig().getInt("setcircles.x044.cost");
			if (cost > 20)
				cost = 20;

			long cdCheck = getSetCircleCooldown(player, "x044");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 0044 (" + setCircleArray.get("0044") + ") again.");
				return;
			}

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			} else if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {

				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}
				Location spawnLoc = effectBlockLocation;

				// check if player has permission to break blocks here first
				if (!checkBlockBreakSimulation(spawnLoc, player)) {
					// player.sendMessage("You don't have permission to do that there.");
					return;
				}

				spawnLoc.add(0.5, 1, 0.5);
				effectBlock.getWorld().spawn(spawnLoc, Pig.class);
				setSetCircleCooldown(player.getName(), "x044", System.currentTimeMillis());
				doEffect = true;
			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Sheep Circle
		else if (arrayString.equals("[0, 1, 4, 4]") && player.hasPermission("geometricmagic.set.0144")) {

			// using x144 because yml doesn't like 0 as first character
			cost = plugin.getConfig().getInt("setcircles.x144.cost");
			if (cost > 20)
				cost = 20;

			long cdCheck = getSetCircleCooldown(player, "x144");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 0144 (" + setCircleArray.get("0144") + ") again.");
				return;
			}

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			} else if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {

				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}
				Location spawnLoc = effectBlockLocation;

				// check if player has permission to break blocks here first
				if (!checkBlockBreakSimulation(spawnLoc, player)) {
					// player.sendMessage("You don't have permission to do that there.");
					return;
				}

				spawnLoc.add(0.5, 1, 0.5);
				effectBlock.getWorld().spawn(spawnLoc, Sheep.class);
				setSetCircleCooldown(player.getName(), "x144", System.currentTimeMillis());
				doEffect = true;
			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Cow Circle
		else if (arrayString.equals("[0, 2, 4, 4]") && player.hasPermission("geometricmagic.set.0244")) {

			// using x244 because yml doesn't like 0 as first character
			cost = plugin.getConfig().getInt("setcircles.x244.cost");
			if (cost > 20)
				cost = 20;

			long cdCheck = getSetCircleCooldown(player, "x244");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 0244 (" + setCircleArray.get("0244") + ") again.");
				return;
			}

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			} else if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {

				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}
				Location spawnLoc = effectBlockLocation;

				// check if player has permission to break blocks here first
				if (!checkBlockBreakSimulation(spawnLoc, player)) {
					// player.sendMessage("You don't have permission to do that there.");
					return;
				}

				spawnLoc.add(0.5, 1, 0.5);
				effectBlock.getWorld().spawn(spawnLoc, Cow.class);
				setSetCircleCooldown(player.getName(), "x244", System.currentTimeMillis());
				doEffect = true;
			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		}

		// Chicken Circle
		else if (arrayString.equals("[0, 3, 4, 4]") && player.hasPermission("geometricmagic.set.0344")) {

			// using x344 because yml doesn't like 0 as first character
			cost = plugin.getConfig().getInt("setcircles.x344.cost");
			if (cost > 20)
				cost = 20;

			long cdCheck = getSetCircleCooldown(player, "x344");
			if (cdCheck > 0) {
				player.sendMessage(ChatColor.RED + "" + cdCheck + " seconds before you can use set circle 0344 (" + setCircleArray.get("0344") + ") again.");
				return;
			}

			if (!hasLearnedCircle(player, arrayString)) {
				player.sendMessage("You have not yet learned circle " + arrayString + "!");
				return;
			} else if (player.getFoodLevel() >= (cost * philosopherStoneModifier(player))) {

				if (!player.hasPermission("geometricmagic.bypass.hunger")) {
					player.setFoodLevel((int) (player.getFoodLevel() - (cost * philosopherStoneModifier(player))));
				}
				Location spawnLoc = effectBlockLocation;

				// check if player has permission to break blocks here first
				if (!checkBlockBreakSimulation(spawnLoc, player)) {
					// player.sendMessage("You don't have permission to do that there.");
					return;
				}

				spawnLoc.add(0.5, 1, 0.5);
				effectBlock.getWorld().spawn(spawnLoc, Chicken.class);
				setSetCircleCooldown(player.getName(), "x344", System.currentTimeMillis());
				doEffect = true;
			} else {
				player.sendMessage("You feel so hungry...");
				return;
			}
		} else {
			player.sendMessage("You do not have permission to use " + arrayString + " or set circle does not exist");
		}
		if (doEffect) {
			doEffect(effectBlockLocation);
		}
	}

	private static void doEffect(Location l) {
		String effectType = plugin.getConfig().getString("effect.type").toLowerCase();
		
		if(effectType.equals("lightning")) {
			// lightning
			l.getWorld().strikeLightningEffect(l);
			
		} else if(effectType.equals("fireworks")) {
			// fireworks
			String firework_type = plugin.getConfig().getString("effect.firework.type").toUpperCase();
			String firework_color = plugin.getConfig().getString("effect.firework.color").toUpperCase();

			Firework fireworks = l.getWorld().spawn(l.add(0.5,1,0.5), Firework.class);
			FireworkMeta data = fireworks.getFireworkMeta();
			data.addEffects(FireworkEffect.builder().withColor(DyeColor.valueOf(firework_color).getColor()).with(Type.valueOf(firework_type)).build());
			data.setPower(plugin.getConfig().getInt("effect.firework.power"));
			fireworks.setFireworkMeta(data);
		}
	}

	private static Material getRepairMaterial(Material type) {
		// TODO make configurable
		// String repairMats = plugin.getConfig().getString("repair.CHAINMAIL_BOOTS");
		// http://wiki.bukkit.org/Configuration_API_Reference#HashMaps
		// Map<String, Object> repairMats = plugin.getConfig().getConfigurationSection("repair").getValues(false);

		if (type == Material.CHAINMAIL_BOOTS)
			return Material.FIRE;
		if (type == Material.CHAINMAIL_CHESTPLATE)
			return Material.FIRE;
		if (type == Material.CHAINMAIL_HELMET)
			return Material.FIRE;
		if (type == Material.CHAINMAIL_LEGGINGS)
			return Material.FIRE;
		if (type == Material.DIAMOND_AXE)
			return Material.DIAMOND;
		if (type == Material.DIAMOND_BOOTS)
			return Material.DIAMOND;
		if (type == Material.DIAMOND_CHESTPLATE)
			return Material.DIAMOND;
		if (type == Material.DIAMOND_HELMET)
			return Material.DIAMOND;
		if (type == Material.DIAMOND_HOE)
			return Material.DIAMOND;
		if (type == Material.DIAMOND_LEGGINGS)
			return Material.DIAMOND;
		if (type == Material.DIAMOND_PICKAXE)
			return Material.DIAMOND;
		if (type == Material.DIAMOND_SPADE)
			return Material.DIAMOND;
		if (type == Material.DIAMOND_SWORD)
			return Material.DIAMOND;
		if (type == Material.IRON_AXE)
			return Material.IRON_INGOT;
		if (type == Material.IRON_BOOTS)
			return Material.IRON_INGOT;
		if (type == Material.IRON_CHESTPLATE)
			return Material.IRON_INGOT;
		if (type == Material.IRON_HELMET)
			return Material.IRON_INGOT;
		if (type == Material.IRON_HOE)
			return Material.IRON_INGOT;
		if (type == Material.IRON_LEGGINGS)
			return Material.IRON_INGOT;
		if (type == Material.IRON_PICKAXE)
			return Material.IRON_INGOT;
		if (type == Material.IRON_SPADE)
			return Material.IRON_INGOT;
		if (type == Material.IRON_SWORD)
			return Material.IRON_INGOT;
		if (type == Material.GOLD_AXE)
			return Material.GOLD_INGOT;
		if (type == Material.GOLD_BOOTS)
			return Material.GOLD_INGOT;
		if (type == Material.GOLD_CHESTPLATE)
			return Material.GOLD_INGOT;
		if (type == Material.GOLD_HELMET)
			return Material.GOLD_INGOT;
		if (type == Material.GOLD_HOE)
			return Material.GOLD_INGOT;
		if (type == Material.GOLD_LEGGINGS)
			return Material.GOLD_INGOT;
		if (type == Material.GOLD_PICKAXE)
			return Material.GOLD_INGOT;
		if (type == Material.GOLD_SPADE)
			return Material.GOLD_INGOT;
		if (type == Material.GOLD_SWORD)
			return Material.GOLD_INGOT;
		if (type == Material.LEATHER_BOOTS)
			return Material.LEATHER;
		if (type == Material.LEATHER_CHESTPLATE)
			return Material.LEATHER;
		if (type == Material.LEATHER_HELMET)
			return Material.LEATHER;
		if (type == Material.LEATHER_LEGGINGS)
			return Material.LEATHER;
		if (type == Material.WOOD_AXE)
			return Material.WOOD;
		if (type == Material.WOOD_HOE)
			return Material.WOOD;
		if (type == Material.WOOD_PICKAXE)
			return Material.WOOD;
		if (type == Material.WOOD_SPADE)
			return Material.WOOD;
		if (type == Material.WOOD_SWORD)
			return Material.WOOD;
		if (type == Material.STONE_AXE)
			return Material.COBBLESTONE;
		if (type == Material.STONE_HOE)
			return Material.COBBLESTONE;
		if (type == Material.STONE_PICKAXE)
			return Material.COBBLESTONE;
		if (type == Material.STONE_SPADE)
			return Material.COBBLESTONE;
		if (type == Material.STONE_SWORD)
			return Material.COBBLESTONE;
		if (type == Material.FLINT_AND_STEEL)
			return Material.IRON_INGOT;
		if (type == Material.SHEARS)
			return Material.IRON_INGOT;
		if (type == Material.FISHING_ROD)
			return Material.STRING;
		if (type == Material.BOW)
			return Material.STRING;
		if (type == Material.CARROT_STICK)
			return Material.CARROT_ITEM;

		return null;
	}

	public static int setCircleSize(Block actBlock) {
		// limit sizes
		int limitsize = plugin.getConfig().getInt("setcircles.limitsize");

		int na = 0, nb = 0, ea = 0, eb = 0, sa = 0, sb = 0, wa = 0, wb = 0, nc = 0, ec = 0, sc = 0, wc = 0;
		Block curBlock = actBlock.getRelative(0, 0, -5);
		while (curBlock.getRelative(0, 0, -1).getType() == Material.REDSTONE_WIRE || na == limitsize) {
			na++;
			curBlock = curBlock.getRelative(0, 0, -1);
		}
		Block fineBlock = curBlock;
		while (fineBlock.getRelative(-1, 0, 0).getType() == Material.REDSTONE_WIRE || nb == limitsize) {
			nb++;
			fineBlock = fineBlock.getRelative(-1, 0, 0);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(1, 0, 0).getType() == Material.REDSTONE_WIRE || nc == limitsize) {
			nc++;
			fineBlock = fineBlock.getRelative(1, 0, 0);
		}
		curBlock = actBlock.getRelative(5, 0, 0);
		while (curBlock.getRelative(1, 0, 0).getType() == Material.REDSTONE_WIRE || ea == limitsize) {
			ea++;
			curBlock = curBlock.getRelative(1, 0, 0);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(0, 0, -1).getType() == Material.REDSTONE_WIRE || eb == limitsize) {
			eb++;
			fineBlock = fineBlock.getRelative(0, 0, -1);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(0, 0, 1).getType() == Material.REDSTONE_WIRE || ec == limitsize) {
			ec++;
			fineBlock = fineBlock.getRelative(0, 0, 1);
		}
		curBlock = actBlock.getRelative(0, 0, 5);
		while (curBlock.getRelative(0, 0, 1).getType() == Material.REDSTONE_WIRE || sa == limitsize) {
			sa++;
			curBlock = curBlock.getRelative(0, 0, 1);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(1, 0, 0).getType() == Material.REDSTONE_WIRE || sb == limitsize) {
			sb++;
			fineBlock = fineBlock.getRelative(1, 0, 0);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(-1, 0, 0).getType() == Material.REDSTONE_WIRE || sc == limitsize) {
			sc++;
			fineBlock = fineBlock.getRelative(-1, 0, 0);
		}
		curBlock = actBlock.getRelative(-5, 0, 0);
		while (curBlock.getRelative(-1, 0, 0).getType() == Material.REDSTONE_WIRE || wa == limitsize) {
			wa++;
			curBlock = curBlock.getRelative(-1, 0, 0);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(0, 0, 1).getType() == Material.REDSTONE_WIRE || wb == limitsize) {
			wb++;
			fineBlock = fineBlock.getRelative(0, 0, 1);
		}
		fineBlock = curBlock;
		while (fineBlock.getRelative(0, 0, -1).getType() == Material.REDSTONE_WIRE || wc == limitsize) {
			wc++;
			fineBlock = fineBlock.getRelative(0, 0, -1);
		}
		int size = 0;
		if (wa == ea && na == sa && wb == eb && nb == sb && wc == ec && nc == sc && wa == na) {
			size = wa;
		}
		return size;
	}

	public static void alchemyCheck(Material a, byte fromData, Material b, byte toData, Location circleStart, Location circleEnd, Location start, Location end, Player player, int width) {
		Block startBlock = circleStart.getBlock();
		int xIteration = 0;
		int yIteration = 0;
		int zIteration = 0;
		if (circleStart.getX() < circleEnd.getX()) {
			if (circleStart.getZ() < circleEnd.getZ()) {
				// east
				// System.out.println("alchemyCheck east");
				while (startBlock.getY() <= circleEnd.getY()) {
					while (startBlock.getX() <= circleEnd.getX()) {
						while (startBlock.getZ() <= circleEnd.getZ()) {
							if (startBlock.getType() != Material.AIR) {
								alchemyFiller(a, fromData, b, toData, start.getBlock().getRelative(xIteration * width, yIteration * width, zIteration * width).getLocation(), start.getBlock().getRelative(xIteration * width + width - 1, yIteration * width + width - 1, (zIteration * width + (width - 1))).getLocation(), player, true);
							}
							zIteration++;
							startBlock = startBlock.getRelative(0, 0, 1);
						}
						xIteration++;
						startBlock = circleStart.getBlock().getRelative(xIteration, yIteration, 0);
						zIteration = 0;
					}
					yIteration++;
					xIteration = 0;
					startBlock = circleStart.getBlock().getRelative(0, yIteration, 0);
				}
			} else {
				// north
				// System.out.println("alchemyCheck north");
				while (startBlock.getY() <= circleEnd.getY()) {
					while (startBlock.getZ() >= circleEnd.getZ()) {
						while (startBlock.getX() <= circleEnd.getX()) {
							if (startBlock.getType() != Material.AIR) {
								alchemyFiller(a, fromData, b, toData, start.getBlock().getRelative(xIteration * width, yIteration * width, zIteration * width).getLocation(), start.getBlock().getRelative(xIteration * width + width - 1, yIteration * width + width - 1, (zIteration * width - (width - 1))).getLocation(), player, true);
							}
							xIteration++;
							// System.out.println("xloop " + xIteration);
							startBlock = startBlock.getRelative(1, 0, 0);
						}
						zIteration--;
						// System.out.println("zloop " + zIteration);
						startBlock = circleStart.getBlock().getRelative(0, yIteration, zIteration);
						xIteration = 0;
					}
					yIteration++;
					// System.out.println("yloop " + yIteration);
					zIteration = 0;
					startBlock = circleStart.getBlock().getRelative(0, yIteration, 0);
				}
			}
		} else {
			if (circleStart.getZ() > circleEnd.getZ()) {
				// west
				// System.out.println("alchemyCheck west");
				while (startBlock.getY() <= circleEnd.getY()) {
					while (startBlock.getX() >= circleEnd.getX()) {
						while (startBlock.getZ() >= circleEnd.getZ()) {
							if (startBlock.getType() != Material.AIR) {
								alchemyFiller(a, fromData, b, toData, start.getBlock().getRelative(xIteration * width, yIteration * width, zIteration * width).getLocation(), start.getBlock().getRelative(xIteration * width - (width - 1), yIteration * width + width - 1, (zIteration * width - (width - 1))).getLocation(), player, true);
							}
							zIteration--;
							startBlock = startBlock.getRelative(0, 0, -1);
						}
						xIteration--;
						startBlock = circleStart.getBlock().getRelative(xIteration, yIteration, 0);
						zIteration = 0;
					}
					yIteration++;
					xIteration = 0;
					startBlock = circleStart.getBlock().getRelative(0, yIteration, 0);
				}
			} else {
				// south
				// System.out.println("alchemyCheck south");
				while (startBlock.getY() <= circleEnd.getY()) {
					while (startBlock.getZ() <= circleEnd.getZ()) {
						while (startBlock.getX() >= circleEnd.getX()) {
							if (startBlock.getType() != Material.AIR) {
								alchemyFiller(a, fromData, b, toData, start.getBlock().getRelative(xIteration * width, yIteration * width, zIteration * width).getLocation(), start.getBlock().getRelative(xIteration * width - (width - 1), yIteration * width + width - 1, (zIteration * width + (width - 1))).getLocation(), player, true);
							}
							xIteration--;
							// System.out.println("xloop");
							startBlock = startBlock.getRelative(-1, 0, 0);
						}
						zIteration++;
						// System.out.println("zloop");
						startBlock = circleStart.getBlock().getRelative(0, yIteration, zIteration);
						xIteration = 0;
					}
					yIteration++;
					// System.out.println("yloop");
					zIteration = 0;
					startBlock = circleStart.getBlock().getRelative(0, yIteration, 0);
				}
			}
		}
		return;
	}

	public static void alchemyFiller(Material a, byte fromData, Material b, byte toData, Location start, Location end, Player player, boolean charge) {
		// System.out.println("alchemyFiller");
		String playerName = player.getName();
		long rate = plugin.getConfig().getLong("transmutation.rate");

		new Thread(new GeometricMagicTransmutationThread(plugin, rate, a, fromData, b, toData, start, end, playerName, charge)).start();
	}

	public static double getBalance(Player player) {

		if (getTransmutationCostSystem(plugin).equalsIgnoreCase("vault")) {
			Economy econ = GeometricMagic.getEconomy();

			double balance = econ.getBalance(player.getName());

			return balance;
		} else if (getTransmutationCostSystem(plugin).equalsIgnoreCase("xp")) {
			double balance = player.getLevel();

			return balance;
		}
		return 0;
	}

	public static double calculatePay(Material a, byte fromData, Material b, byte toData, Player player) {
		// TODO: create a power system similar to mcMMO to level alchemy 

		double pay = (getBlockValue(plugin, a.getId(), (int) fromData, (short) 0, "sell", player) - getBlockValue(plugin, b.getId(), (int) toData, (short) 0, "buy", player));

		// Apply Philosopher's Stone to transmutes config variable
		String stoneConfig = plugin.getConfig().getString("transmutation.stone");
		if (stoneConfig == "true") {
			return (double) (pay * philosopherStoneModifier(player));
		} else {
			return pay;
		}
	}

	public static double philosopherStoneModifier(Player player) {
		double modifier = 1;
		int stackCount = 0;
		PlayerInventory inventory = player.getInventory();
		for (int i = 0; i < inventory.getSize(); i++) {
			if (inventory.getItem(i) != null && inventory.getItem(i).getType() == Material.PORTAL)
				stackCount += inventory.getItem(i).getAmount();
		}
		int maxPStone = plugin.getConfig().getInt("philosopherstone.max");
		if (stackCount > maxPStone) {
			stackCount = maxPStone;
		}

		float multiplierModifier = (float) plugin.getConfig().getDouble("philosopherstone.modifier");

		modifier = 1 / (Math.pow(2, stackCount) * multiplierModifier);
		return modifier;
	}

	public static void humanTransmutation(Player player) throws IOException {
		if (new File("plugins/GeometricMagic/").mkdirs())
			System.out.println("[GeometricMagic] Sacrifices file created.");
		File myFile = new File("plugins/GeometricMagic/sacrifices.txt");
		if (myFile.exists()) {
			Scanner inputFile = new Scanner(myFile);
			while (inputFile.hasNextLine()) {
				String name = inputFile.nextLine();
				if (name.equals(player.getName())) {
					FileWriter dWriter = new FileWriter("plugins/GeometricMagic/sacrificed.txt", true);
					PrintWriter dFile = new PrintWriter(dWriter);
					dFile.println(player.getName());
					dFile.close();
					return;
				}
			}
			inputFile.close();
		} else {
			PrintWriter outputFile = new PrintWriter("plugins/GeometricMagic/sacrifices.txt");
			System.out.println("[GeometricMagic] Sacrifices file created.");
			outputFile.close();
		}
		FileWriter fWriter = new FileWriter("plugins/GeometricMagic/sacrifices.txt", true);
		PrintWriter outputFile = new PrintWriter(fWriter);
		outputFile.println(player.getName());
		outputFile.println(0);
		player.sendMessage("You have committed the taboo! Crafting is your sacrifice, knowledge your reward.");
		outputFile.close();
	}

	public static boolean hasLearnedCircle(Player player, String circle) throws IOException {
		File myFile = new File("plugins/GeometricMagic/" + player.getName() + ".txt");
		if (!myFile.exists()) {
			return false;
		}
		Scanner inputFile = new Scanner(myFile);
		while (inputFile.hasNextLine()) {
			String name = inputFile.nextLine();
			if (name.equals(circle)) {
				inputFile.close();
				return true;
			}
		}
		inputFile.close();
		return false;
	}

	public static boolean learnCircle(Player player, String circle, Block actBlock) throws IOException {
		boolean status = false;
		// System.out.println("learnCircle");
		ItemStack oneRedstone = new ItemStack(331, 1);
		Item redStack = actBlock.getWorld().dropItem(actBlock.getLocation(), oneRedstone);
		List<Entity> entityList = redStack.getNearbyEntities(2, 10, 2);
		for (int i = 0; i < entityList.size(); i++) {
			if (entityList.get(i) instanceof Enderman) {
				if (new File("plugins/GeometricMagic/").mkdirs())
					System.out.println("[GeometricMagic] File created for " + player.getName());
				File myFile = new File("plugins/GeometricMagic/" + player.getName() + ".txt");
				if (myFile.exists()) {
					FileWriter fWriter = new FileWriter("plugins/GeometricMagic/" + player.getName() + ".txt", true);
					PrintWriter outputFile = new PrintWriter(fWriter);
					outputFile.println(circle);
					outputFile.close();
				} else {
					PrintWriter outputFile = new PrintWriter("plugins/GeometricMagic/" + player.getName() + ".txt");
					outputFile.println(circle);
					outputFile.close();
				}
				status = true;
				setSetCircleCooldown(player.getName(), "learn", System.currentTimeMillis());
			}
		}
		redStack.remove();
		return status;
	}

	public static void storageCircle(Location startLoc, Location endLoc, Player player, int size) {

		File folder = new File("plugins/GeometricMagic/storage/");
		File file = new File("plugins/GeometricMagic/storage/" + player.getName() + "." + String.valueOf(size));
		if (folder.exists()) {
			// Load blocks
			if (file.exists()) {
				try {
					// exempt player from AntiCheat check
					if (Bukkit.getServer().getPluginManager().getPlugin("AntiCheat") != null) {
						AnticheatAPI.exemptPlayer(player, CheckType.FAST_PLACE);
						AnticheatAPI.exemptPlayer(player, CheckType.FAST_BREAK);
						AnticheatAPI.exemptPlayer(player, CheckType.LONG_REACH);
						AnticheatAPI.exemptPlayer(player, CheckType.NO_SWING);
					}

					storageCircleLoad(startLoc, endLoc, player, size, file);

					// unexempt player from AntiCheat check
					if (Bukkit.getServer().getPluginManager().getPlugin("AntiCheat") != null) {
						AnticheatAPI.unexemptPlayer(player, CheckType.FAST_PLACE);
						AnticheatAPI.unexemptPlayer(player, CheckType.FAST_BREAK);
						AnticheatAPI.unexemptPlayer(player, CheckType.LONG_REACH);
						AnticheatAPI.unexemptPlayer(player, CheckType.NO_SWING);
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
			// Store blocks
			else {
				try {
					// exempt player from AntiCheat check
					if (Bukkit.getServer().getPluginManager().getPlugin("AntiCheat") != null) {
						AnticheatAPI.exemptPlayer(player, CheckType.FAST_PLACE);
						AnticheatAPI.exemptPlayer(player, CheckType.FAST_BREAK);
						AnticheatAPI.exemptPlayer(player, CheckType.LONG_REACH);
						AnticheatAPI.exemptPlayer(player, CheckType.NO_SWING);
					}

					storageCircleStore(startLoc, endLoc, player, size, file);

					// unexempt player from AntiCheat check
					if (Bukkit.getServer().getPluginManager().getPlugin("AntiCheat") != null) {
						AnticheatAPI.unexemptPlayer(player, CheckType.FAST_PLACE);
						AnticheatAPI.unexemptPlayer(player, CheckType.FAST_BREAK);
						AnticheatAPI.unexemptPlayer(player, CheckType.LONG_REACH);
						AnticheatAPI.unexemptPlayer(player, CheckType.NO_SWING);
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		} else {
			// Store blocks
			if (folder.mkdirs()) {
				try {
					// exempt player from AntiCheat check
					if (Bukkit.getServer().getPluginManager().getPlugin("AntiCheat") != null) {
						AnticheatAPI.exemptPlayer(player, CheckType.FAST_PLACE);
						AnticheatAPI.exemptPlayer(player, CheckType.FAST_BREAK);
						AnticheatAPI.exemptPlayer(player, CheckType.LONG_REACH);
						AnticheatAPI.exemptPlayer(player, CheckType.NO_SWING);
					}

					storageCircleStore(startLoc, endLoc, player, size, file);

					// unexempt player from AntiCheat check
					if (Bukkit.getServer().getPluginManager().getPlugin("AntiCheat") != null) {
						AnticheatAPI.unexemptPlayer(player, CheckType.FAST_PLACE);
						AnticheatAPI.unexemptPlayer(player, CheckType.FAST_BREAK);
						AnticheatAPI.unexemptPlayer(player, CheckType.LONG_REACH);
						AnticheatAPI.unexemptPlayer(player, CheckType.NO_SWING);
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			} else
				System.out.println("[GeometricMagic] Error creating necessary folder(s)!" + " Check your read/write permissions");
		}
	}

	public static void storageCircleLoad(Location startLoc, Location endLoc, Player player, int size, File file) throws FileNotFoundException {
		World world = player.getWorld();
		Scanner in = new Scanner(file);
		plugin.getServer().getPluginManager().registerEvents(blockListener, plugin);

		if (startLoc.getBlockX() > endLoc.getBlockX()) {
			if (startLoc.getBlockZ() > endLoc.getBlockZ()) {
				for (int x = startLoc.getBlockX(); x >= endLoc.getBlockX(); x--) {
					for (int y = startLoc.getBlockY(); y <= endLoc.getBlockY(); y++) {
						for (int z = startLoc.getBlockZ(); z >= endLoc.getBlockZ(); z--) {
							Location loc = new Location(world, x, y, z);
							Block block = loc.getBlock();
							String newBlockString = in.next();
							String[] newBlockStringArray = newBlockString.split(",");
							int newBlockID = Integer.parseInt(newBlockStringArray[0]);
							byte newBlockData = Byte.parseByte(newBlockStringArray[1]);

							// Block break
							if (block.getTypeId() != 0 && newBlockID == 0) {
								if (!checkBreakBlacklist(block.getTypeId())) {
									if (checkBlockBreakSimulation(loc, player)) {
										block.setTypeIdAndData(newBlockID, newBlockData, false);
									}
								}
							}
							// Block place
							else if (block.getTypeId() == 0 && newBlockID != 0) {
								if (!checkPlaceBlacklist(newBlockID)) {
									if (checkBlockPlaceSimulation(loc, newBlockID, newBlockData, loc, player)) {
										block.setTypeIdAndData(newBlockID, newBlockData, false);
									}
								}
							}
							// Block break and place
							else if (block.getTypeId() != 0 && newBlockID != 0) {
								if (!checkBreakBlacklist(block.getTypeId()) && !checkPlaceBlacklist(newBlockID)) {
									if (checkBlockBreakSimulation(loc, player) && checkBlockPlaceSimulation(loc, newBlockID, newBlockData, loc, player)) {
										block.setTypeIdAndData(newBlockID, newBlockData, false);
									}
								}
							}
						}
					}
				}
			} else {
				for (int z = startLoc.getBlockZ(); z <= endLoc.getBlockZ(); z++) {
					for (int y = startLoc.getBlockY(); y <= endLoc.getBlockY(); y++) {
						for (int x = startLoc.getBlockX(); x >= endLoc.getBlockX(); x--) {
							Location loc = new Location(world, x, y, z);
							Block block = loc.getBlock();
							String newBlockString = in.next();
							String[] newBlockStringArray = newBlockString.split(",");
							int newBlockID = Integer.parseInt(newBlockStringArray[0]);
							byte newBlockData = Byte.parseByte(newBlockStringArray[1]);

							// Block break
							if (block.getTypeId() != 0 && newBlockID == 0) {
								if (!checkBreakBlacklist(block.getTypeId())) {
									if (checkBlockBreakSimulation(loc, player)) {
										block.setTypeIdAndData(newBlockID, newBlockData, false);
									}
								}
							}
							// Block place
							else if (block.getTypeId() == 0 && newBlockID != 0) {
								if (!checkPlaceBlacklist(newBlockID)) {
									if (checkBlockPlaceSimulation(loc, newBlockID, newBlockData, loc, player)) {
										block.setTypeIdAndData(newBlockID, newBlockData, false);
									}
								}
							}
							// Block break and place
							else if (block.getTypeId() != 0 && newBlockID != 0) {
								if (!checkBreakBlacklist(block.getTypeId()) && !checkPlaceBlacklist(newBlockID)) {
									if (checkBlockBreakSimulation(loc, player) && checkBlockPlaceSimulation(loc, newBlockID, newBlockData, loc, player)) {
										block.setTypeIdAndData(newBlockID, newBlockData, false);
									}
								}
							}
						}
					}
				}
			}
		} else {
			if (startLoc.getBlockZ() > endLoc.getBlockZ()) {
				for (int z = startLoc.getBlockZ(); z >= endLoc.getBlockZ(); z--) {
					for (int y = startLoc.getBlockY(); y <= endLoc.getBlockY(); y++) {
						for (int x = startLoc.getBlockX(); x <= endLoc.getBlockX(); x++) {
							Location loc = new Location(world, x, y, z);
							Block block = loc.getBlock();
							String newBlockString = in.next();
							String[] newBlockStringArray = newBlockString.split(",");
							int newBlockID = Integer.parseInt(newBlockStringArray[0]);
							byte newBlockData = Byte.parseByte(newBlockStringArray[1]);

							// Block break
							if (block.getTypeId() != 0 && newBlockID == 0) {
								if (!checkBreakBlacklist(block.getTypeId())) {
									if (checkBlockBreakSimulation(loc, player)) {
										block.setTypeIdAndData(newBlockID, newBlockData, false);
									}
								}
							}
							// Block place
							else if (block.getTypeId() == 0 && newBlockID != 0) {
								if (!checkPlaceBlacklist(newBlockID)) {
									if (checkBlockPlaceSimulation(loc, newBlockID, newBlockData, loc, player)) {
										block.setTypeIdAndData(newBlockID, newBlockData, false);
									}
								}
							}
							// Block break and place
							else if (block.getTypeId() != 0 && newBlockID != 0) {
								if (!checkBreakBlacklist(block.getTypeId()) && !checkPlaceBlacklist(newBlockID)) {
									if (checkBlockBreakSimulation(loc, player) && checkBlockPlaceSimulation(loc, newBlockID, newBlockData, loc, player)) {
										block.setTypeIdAndData(newBlockID, newBlockData, false);
									}
								}
							}
						}
					}
				}
			} else {
				for (int x = startLoc.getBlockX(); x <= endLoc.getBlockX(); x++) {
					for (int y = startLoc.getBlockY(); y <= endLoc.getBlockY(); y++) {
						for (int z = startLoc.getBlockZ(); z <= endLoc.getBlockZ(); z++) {
							Location loc = new Location(world, x, y, z);
							Block block = loc.getBlock();
							String newBlockString = in.next();
							String[] newBlockStringArray = newBlockString.split(",");
							int newBlockID = Integer.parseInt(newBlockStringArray[0]);
							byte newBlockData = Byte.parseByte(newBlockStringArray[1]);

							// Block break
							if (block.getTypeId() != 0 && newBlockID == 0) {
								if (!checkBreakBlacklist(block.getTypeId())) {
									if (checkBlockBreakSimulation(loc, player)) {
										block.setTypeIdAndData(newBlockID, newBlockData, false);
									}
								}
							}
							// Block place
							else if (block.getTypeId() == 0 && newBlockID != 0) {
								if (!checkPlaceBlacklist(newBlockID)) {
									if (checkBlockPlaceSimulation(loc, newBlockID, newBlockData, loc, player)) {
										block.setTypeIdAndData(newBlockID, newBlockData, false);
									}
								}
							}
							// Block break and place
							else if (block.getTypeId() != 0 && newBlockID != 0) {
								if (!checkBreakBlacklist(block.getTypeId()) && !checkPlaceBlacklist(newBlockID)) {
									if (checkBlockBreakSimulation(loc, player) && checkBlockPlaceSimulation(loc, newBlockID, newBlockData, loc, player)) {
										block.setTypeIdAndData(newBlockID, newBlockData, false);
									}
								}
							}
						}
					}
				}
			}
		}
		HandlerList.unregisterAll(blockListener);
		in.close();
		file.delete();
	}

	public static void storageCircleStore(Location startLoc, Location endLoc, Player player, int size, File file) throws FileNotFoundException {
		World world = player.getWorld();
		PrintWriter out = new PrintWriter(file);
		plugin.getServer().getPluginManager().registerEvents(blockListener, plugin);

		if (startLoc.getBlockX() > endLoc.getBlockX()) {
			if (startLoc.getBlockZ() > endLoc.getBlockZ()) {
				for (int x = startLoc.getBlockX(); x >= endLoc.getBlockX(); x--) {
					for (int y = startLoc.getBlockY(); y <= endLoc.getBlockY(); y++) {
						for (int z = startLoc.getBlockZ(); z >= endLoc.getBlockZ(); z--) {
							Location loc = new Location(world, x, y, z);
							Block block = loc.getBlock();

							if (block.getType() != Material.AIR) {
								if (!checkBreakBlacklist(block.getTypeId())) {
									if (checkBlockBreakSimulation(loc, player)) {
										out.println(String.valueOf(block.getTypeId()) + "," + String.valueOf(block.getData()));
										block.setTypeId(0, false);
									}
								} else {
									out.println("0,0");
								}
							} else {
								out.println("0,0");
							}
						}
					}
				}
			} else {
				for (int z = startLoc.getBlockZ(); z <= endLoc.getBlockZ(); z++) {
					for (int y = startLoc.getBlockY(); y <= endLoc.getBlockY(); y++) {
						for (int x = startLoc.getBlockX(); x >= endLoc.getBlockX(); x--) {
							Location loc = new Location(world, x, y, z);
							Block block = loc.getBlock();

							if (block.getType() != Material.AIR) {
								if (!checkBreakBlacklist(block.getTypeId())) {
									if (checkBlockBreakSimulation(loc, player)) {
										out.println(String.valueOf(block.getTypeId()) + "," + String.valueOf(block.getData()));
										block.setTypeId(0, false);
									}
								} else {
									out.println("0,0");
								}
							} else {
								out.println("0,0");
							}
						}
					}
				}
			}
		} else {
			if (startLoc.getBlockZ() > endLoc.getBlockZ()) {
				for (int z = startLoc.getBlockZ(); z >= endLoc.getBlockZ(); z--) {
					for (int y = startLoc.getBlockY(); y <= endLoc.getBlockY(); y++) {
						for (int x = startLoc.getBlockX(); x <= endLoc.getBlockX(); x++) {
							Location loc = new Location(world, x, y, z);
							Block block = loc.getBlock();

							if (block.getType() != Material.AIR) {
								if (!checkBreakBlacklist(block.getTypeId())) {
									if (checkBlockBreakSimulation(loc, player)) {
										out.println(String.valueOf(block.getTypeId()) + "," + String.valueOf(block.getData()));
										block.setTypeId(0, false);
									}
								} else {
									out.println("0,0");
								}
							} else {
								out.println("0,0");
							}
						}
					}
				}
			} else {
				for (int x = startLoc.getBlockX(); x <= endLoc.getBlockX(); x++) {
					for (int y = startLoc.getBlockY(); y <= endLoc.getBlockY(); y++) {
						for (int z = startLoc.getBlockZ(); z <= endLoc.getBlockZ(); z++) {
							Location loc = new Location(world, x, y, z);
							Block block = loc.getBlock();

							if (block.getType() != Material.AIR) {
								if (!checkBreakBlacklist(block.getTypeId())) {
									if (checkBlockBreakSimulation(loc, player)) {
										out.println(String.valueOf(block.getTypeId()) + "," + String.valueOf(block.getData()));
										block.setTypeId(0, false);
									}
								} else {
									out.println("0,0");
								}
							} else {
								out.println("0,0");
							}
						}
					}
				}
			}
		}
		HandlerList.unregisterAll(blockListener);
		out.close();
	}

	public static String getTransmutationCostSystem(GeometricMagic plugin) {
		return plugin.getConfig().getString("transmutation.cost").toString();
	}

	public static double getBlockValue(GeometricMagic plugin, int ID, int Data, short durability, String buyOrSell, Player player) {
		
		// TODO: create a power system similar to mcMMO to level alchemy 

		double value = 0;
		Integer multiplier = 1;

		// translate items with different ids or data values when placed
		// http://www.minecraftwiki.net/wiki/Data_values#Data
		switch (ID) {
		case 0:
			// air
			return 0;
		case 29:
			// sticky piston
			Data = 7;
			durability = 7;
			break;
		case 33:
			// piston
			Data = 7;
			durability = 7;
			break;
		case 34:
			// piston extension
			ID = 33;
			Data = 7;
			durability = 7;
			break;
		case 23:
			// dispenser
			Data = 0;
			break;
		case 55:
			// redstone wire
			ID = 331;
			Data = 0;
			break;
		case 93:
			// repeater off
			ID = 356;
			Data = 0;
			break;
		case 94:
			// repeater on
			ID = 356;
			Data = 0;
			break;
		case 69:
			// lever
			Data = 0;
			break;
		case 75:
			// redstone torch off
			Data = 0;
			break;
		case 76:
			// redstone torch on
			Data = 0;
			break;
		case 77:
			// button
			Data = 0;
			break;
		case 96:
			// trapdoor
			Data = 0;
			break;
		case 131:
			// tripwire hook
			Data = 0;
			break;
		case 64:
			// wooden door
			ID = 324;
			Data = 0;
			break;
		case 71:
			// iron door
			ID = 268;
			Data = 0;
			break;
		case 107:
			// fence gate
			Data = 0;
			break;
		case 70:
			// wooden pressureplate
			Data = 0;
			break;
		case 72:
			// stone pressureplate
			Data = 0;
			break;
		case 43:
			// doublestep
			ID = 44;
			multiplier = 2;
			break;
		case 125:
			// wooden doublestep
			ID = 126;
			multiplier = 2;
			break;
		case 53:
			// oak wood stairs
			Data = 0;
			break;
		case 67:
			// cobblestone stairs
			Data = 0;
			break;
		case 108:
			// brick stairs
			Data = 0;
			break;
		case 109:
			// smooth brich stairs
			Data = 0;
			break;
		case 114:
			// nether brick stairs
			Data = 0;
			break;
		case 128:
			// sandstone stairs
			Data = 0;
			break;
		case 134:
			// spruce wood stairs
			Data = 0;
			break;
		case 135:
			// birch wood stairs
			Data = 0;
			break;
		case 136:
			// jungle wood stairs
			Data = 0;
			break;
		case 86:
			// pumpkin
			Data = 0;
			break;
		case 91:
			// jack'o'lantern
			Data = 0;
			break;
		case 54:
			// chest
			Data = 0;
			break;
		case 61:
			// furnace
			Data = 0;
			break;
		case 84:
			// jukebox
			Data = 0;
			break;
		case 65:
			// ladder
			Data = 0;
			break;
		case 63:
			// sign
			ID = 323;
			Data = 0;
			break;
		case 130:
			// ender chest
			Data = 0;
			break;
		case 106:
			// vines
			Data = 0;
			break;
		case 26:
			// bed
			ID = 355;
			Data = 0;
			break;
		case 124:
			// redstone lamp on
			ID = 123;
			Data = 0;
			break;
		case 27:
			// powered rail
			Data = 0;
			break;
		case 28:
			// detector rail
			Data = 0;
			break;
		case 66:
			// minecraft track
			Data = 0;
			break;
		case 118:
			// cauldron
			ID = 380;
			Data = 0;
			break;
		case 117:
			// brewing stand
			ID = 379;
			Data = 0;
			break;
		case 17:
			// logs
			if (Data == 8)
				Data = 0;
			if (Data == 9)
				Data = 1;
			if (Data == 10)
				Data = 2;
			if (Data == 11)
				Data = 3;
			break;
		case 18:
			// leaves
			if (Data == 12 || Data == 4) {
				Data = 4;
				durability = 4;
			}
			if (Data == 13 || Data == 5) {
				Data = 5;
				durability = 5;
			}
			if (Data == 14 || Data == 6) {
				Data = 6;
				durability = 6;
			}
			if (Data == 15 || Data == 7) {
				Data = 7;
				durability = 7;
			}
			break;
		case 132:
			// tripwire
			ID = 287;
			Data = 0;
			break;
		case 9:
			// water
			ID = 326;
			Data = 0;
			break;
		case 10:
			// lava
			ID = 327;
			Data = 0;
			break;
		case 11:
			// lava
			ID = 327;
			Data = 0;
			break;
		case 78:
			// snow
			ID = 332;
			Data = 0;
			break;
		case 51:
			// fire
			Data = 0;
			break;
		case 31:
			// grass
			Data = 1;
			durability = 1;
			break;
		case 92:
			// cake
			ID = 354;
			Data = 0;
			break;
		case 83:
			// sugarcane
			ID = 338;
			Data = 0;
			break;
		case 104:
			// pumpkin sprout
			ID = 361;
			Data = 0;
			durability = 0;
			break;
		case 105:
			// melon sprout
			ID = 362;
			Data = 0;
			durability = 0;
			break;
		case 59:
			// seeds
			ID = 295;
			Data = 0;
			durability = 0;
			break;
		case 60:
			// soil
			ID = 3;
			Data = 0;
			durability = 0;
			break;
		case 127:
			// cocoa beans
			ID = 351;
			Data = 3;
			durability = 3;
			break;
		case 115:
			// nether wart
			ID = 317;
			Data = 0;
			durability = 0;
			break;
		case 144:
			// heads
			ID = 397;
			break;
		default:
			break;
		}

		if (plugin.getConfig().getString("valuesource").equals("hyperconomy") && Bukkit.getPluginManager().isPluginEnabled("Hyperconomy")) {

			HyperObjectAPI hyperApi = new HyperObjectAPI();

			if (buyOrSell == "buy") {
				value = (double) hyperApi.getTruePurchasePrice(ID, durability, 1, plugin.getConfig().getString("hyperconomy.economy")) * multiplier;
			} else if (buyOrSell == "sell") {
				value = (double) hyperApi.getTrueSaleValue(ID, durability, 1, player) * multiplier;
			} else {
				return 0;
			}

		} else if (plugin.getConfig().getString("valuesource").equals("dynamiceconomy")) {
			// Dynamic Economy support not yet implemented (plugin is outdated).
			value = plugin.getConfig().getInt("values." + ID + "." + Data);
		} else {
			value = plugin.getConfig().getInt("values." + ID + "." + Data);
		}

		return value;
	}

	// Lyneira's Code Start
	public static boolean checkBlockPlaceSimulation(Location target, int typeId, byte data, Location placedAgainst, Player player) {
		Block placedBlock = target.getBlock();
		BlockState replacedBlockState = placedBlock.getState();
		int oldType = replacedBlockState.getTypeId();
		byte oldData = replacedBlockState.getRawData();

		// Set the new state without physics.
		placedBlock.setTypeIdAndData(typeId, data, false);
		BlockPlaceEvent placeEvent = new BlockPlaceEvent(placedBlock, replacedBlockState, placedAgainst.getBlock(), null, player, true);
		getPluginManager().callEvent(placeEvent);

		// Revert to the old state without physics.
		placedBlock.setTypeIdAndData(oldType, oldData, false);
		if (placeEvent.isCancelled()) {
			return false;
		} else {
			placeEvent.setCancelled(true);
			return true;
		}
	}

	public static boolean checkBlockBreakSimulation(Location target, Player player) {
		Block block = target.getBlock();
		BlockBreakEvent breakEvent = new BlockBreakEvent(block, player);
		getPluginManager().callEvent(breakEvent);
		if (breakEvent.isCancelled()) {
			return false;
		} else {
			breakEvent.setCancelled(true);
			return true;
		}
	}

	// Lyneira's Code End

	public static boolean checkBreakBlacklist(int m) {
		List<String> blacklist = plugin.getConfig().getStringList("blacklist.break");
		for (String s : blacklist) {
			if (s.equals(String.valueOf(m)))
				return true;
		}
		return false;
	}

	public static boolean checkPlaceBlacklist(int m) {
		List<String> blacklist = plugin.getConfig().getStringList("blacklist.place");
		for (String s : blacklist) {
			if (s.equals(String.valueOf(m)))
				return true;
		}
		return false;
	}

	public static PluginManager getPluginManager() {
		return plugin.getServer().getPluginManager();
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onCraftItem (CraftItemEvent event) {
		ItemStack newItemStack = event.getRecipe().getResult();
		
		// if the new item is a portal
		if (newItemStack.getType() == Material.PORTAL) {
			
			// check permissions
			if (!event.getWhoClicked().hasPermission("geometricmagic.set.2223")) {
				event.setCancelled(true);
			}
		}
	}

}
